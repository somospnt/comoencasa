SET foreign_key_checks = 0;

DROP TABLE IF EXISTS `cec_cliente`;
CREATE TABLE IF NOT EXISTS `cec_cliente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_casa` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `notas` text,
  `fecha_alta` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `cec_cliente_ibfk_1` (`id_casa`),
  KEY `cec_cliente_ibfk_2` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1;

ALTER TABLE `cec_cliente`
  ADD CONSTRAINT `cec_cliente_ibfk_1` FOREIGN KEY (`id_casa`) REFERENCES `cec_casa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cec_cliente_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `cec_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
  
SET foreign_key_checks = 1;