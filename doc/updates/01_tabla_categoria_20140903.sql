/**
* 
* Creación de tabla Categoría y sus valores precargados.
*
*/

CREATE TABLE IF NOT EXISTS `cec_categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

ALTER table `cec_menu`
ADD COLUMN `id_categoria` int(11) NULL;

ALTER TABLE  `cec_menu` ADD INDEX (  `id_categoria` );

ALTER TABLE  `cec_menu` ADD FOREIGN KEY (  `id_categoria` ) REFERENCES  `comoencasa`.`cec_categoria` (`id`) 
ON DELETE CASCADE ON UPDATE CASCADE ;

INSERT INTO `cec_categoria` (`id`, `nombre`) VALUES
(1, 'Carnes'),
(2, 'Pollos'),
(3, 'Pescados'),
(4, 'Pastas'),
(5, 'Vegetarianas'),
(6, 'Tartas'),
(7, 'Meriendas');