-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 22-08-2014 a las 21:22:46
-- Versión del servidor: 5.5.16
-- Versión de PHP: 5.3.8

SET foreign_key_checks = 0;

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `comoencasa`
--

--
-- Volcado de datos para la tabla `cec_casa`
--

INSERT INTO `cec_casa` (`id`, `nombre`, `codigo`, `mail`, `descripcion`, `imagen`) VALUES
(1, 'KalCero', 'kalcero', 'info@kalcero.com', 'Te ofrecemos un servicio a domicilio de viandas de cocina casera para dietas restringidas en calorías, o simplemente para que puedas dejar de cocinar.', 'logo.jpg'),
(2, 'Ratatouille', 'ratatouille', 'info@ratatouille.com', NULL, 'logo.jpg'),
(3, 'Oh! Sushi', 'oh-sushi', 'info@ohsushi.com', 'Japanese restaurante & delivery', 'logo.jpg'),
(4, 'TGI Seba', 'tgi-seba', 'info@tgiseba.com', 'Deliciosa comida. 100% Grasas trans. 100% Colesterol. Muertos de placer.', 'logo.jpg');

--
-- Volcado de datos para la tabla `cec_menu`
--

INSERT INTO `cec_menu` (`id`, `nombre`, `descripcion`, `id_casa`, `imagen`) VALUES
(1, 'Vianda vegana', 'Vianda vegana descripción.', 1, 'vianda_1.jpg'),
(2, 'Vianda oriental', 'Vianda oriental descripcion.', 1, 'vianda_2.jpg'),
(3, 'Vianda saludable', 'Vianda saludable descripción.', 1, 'vianda_3.jpg'),
(4, 'Vianda promo', 'Vianda promo descripción.', 2, 'vianda_4.jpg'),
(5, 'Vianda pan de cada dia', 'Comete la flautita', 2, 'vianda_5.jpg'),
(6, 'Revuelto bomba', 'Aprovechá esta receta finamente aceitada', 4, 'vianda_1.jpg'),
(7, 'Super hamburguesa "higado destroy"', 'Manjar chorreante', 4, 'vianda_2.jpg'),
(8, 'Verduritas para tu novia', 'Si ella no quiere romperse, comprale esto.', 4, 'vianda_3.jpg'),
(9, 'Carne humana 100%', 'Deliciosa carne de africanos con alzheimer', 4, 'vianda_4.jpg'),
(10, 'Combinado Tokyo', 'Niguiri, Piriki, Chiniri, Putini', 3, 'vianda_5.jpg'),
(11, 'Combinado Pakarta', 'Roll, Croll, Polls, Dance', 3, 'vianda_7.jpg');

--
-- Volcado de datos para la tabla `cec_pedido`
--

INSERT INTO `cec_pedido` (`id`, `fecha`, `id_casa`, `id_usuario`, `estado`) VALUES
(1, '2014-08-15', 1, 2, 'ABIERTO');

--
-- Volcado de datos para la tabla `cec_pedido_item`
--

INSERT INTO `cec_pedido_item` (`id`, `id_menu`, `id_pedido`, `cantidad`) VALUES
(1, 1, 1, 10),
(2, 2, 1, 10);

--
-- Volcado de datos para la tabla `cec_usuario`
--

INSERT INTO `cec_usuario` (`id`, `nombre`, `direccion`, `telefono`, `mail`) VALUES
(1, 'Maro', 'Calle Falsa 123', '123456789', 'maro@mail.com'),
(2, 'Gerard', 'Calle Falsa 123', '123456789', 'Gerard@mail.com');

INSERT INTO `cec_categoria` (`id`, `nombre`) VALUES
(1, 'Carnes'),
(2, 'Pollos'),
(3, 'Pescados'),
(4, 'Pastas'),
(5, 'Vegetarianas'),
(6, 'Tartas'),
(7, 'Meriendas');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

SET foreign_key_checks = 1;