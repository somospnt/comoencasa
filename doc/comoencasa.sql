SET foreign_key_checks = 0;


DROP TABLE IF EXISTS `cec_casa`;
CREATE TABLE IF NOT EXISTS `cec_casa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `codigo` varchar(255) NOT NULL,
  `mail` varchar(255) DEFAULT NULL,
  `descripcion` text,
  `imagen` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `cec_menu`;
CREATE TABLE IF NOT EXISTS `cec_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  `descripcion` text,
  `id_casa` int(11) NOT NULL,
  `id_categoria` int(11) NULL,
  `imagen` varchar(255) DEFAULT NULL,
  `visible` BOOLEAN NOT NULL DEFAULT  '1',
  PRIMARY KEY (`id`),
  KEY `id_casa` (`id_casa`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `cec_pedido`;
CREATE TABLE IF NOT EXISTS `cec_pedido` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha` date NOT NULL,
  `id_casa` int(11) NOT NULL,
  `id_usuario` int(11) NOT NULL,
  `estado` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_casa` (`id_casa`),
  KEY `id_usuario` (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `cec_pedido_item`;
CREATE TABLE IF NOT EXISTS `cec_pedido_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_menu` int(11) NOT NULL,
  `id_pedido` int(11) NOT NULL,
  `cantidad` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_menu` (`id_menu`),
  KEY `id_pedido` (`id_pedido`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `cec_usuario`;
CREATE TABLE IF NOT EXISTS `cec_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_casa` int(11),
  `nombre` varchar(255) NOT NULL,
  `direccion` varchar(255) DEFAULT NULL,
  `telefono` varchar(255) DEFAULT NULL,
  `mail` varchar(255) NOT NULL,
  `password` varchar(255),
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

DROP TABLE IF EXISTS `cec_rol`;
CREATE TABLE IF NOT EXISTS `cec_rol` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_usuario` int(11) NOT NULL,
  `rol` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `usuario_id` (`id_usuario`,`rol`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


ALTER TABLE `cec_menu`
  ADD CONSTRAINT `cec_menu_ibfk_1` FOREIGN KEY (`id_casa`) REFERENCES `cec_casa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cec_menu_ibfk_2` FOREIGN KEY (  `id_categoria` ) REFERENCES  `cec_categoria` (`id`) ON DELETE CASCADE ON UPDATE CASCADE ;

ALTER TABLE `cec_pedido`
  ADD CONSTRAINT `cec_pedido_ibfk_1` FOREIGN KEY (`id_casa`) REFERENCES `cec_casa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cec_pedido_ibfk_2` FOREIGN KEY (`id_usuario`) REFERENCES `cec_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `cec_pedido_item`
  ADD CONSTRAINT `cec_pedido_item_ibfk_1` FOREIGN KEY (`id_menu`) REFERENCES `cec_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `cec_pedido_item_ibfk_2` FOREIGN KEY (`id_pedido`) REFERENCES `cec_pedido` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE  `cec_casa` ADD UNIQUE (
`codigo`
);

ALTER TABLE  `cec_usuario` ADD UNIQUE (
    mail
);

ALTER TABLE `cec_rol`
  ADD CONSTRAINT `cec_rol_ibfk_1` FOREIGN KEY (`id_usuario`) REFERENCES `cec_usuario` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `cec_usuario`
  ADD CONSTRAINT `cec_usuario_ibfk_1` FOREIGN KEY (`id_casa`) REFERENCES `cec_casa` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

DROP TABLE IF EXISTS `cec_categoria`;
CREATE TABLE IF NOT EXISTS `cec_categoria` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;


SET foreign_key_checks = 1;
