package comoencasa.controller;

import comoencasa.domain.Menu;
import comoencasa.service.MenuService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MenuRestController {

    @Autowired
    private MenuService menuService;

    @RequestMapping(value = "/api/menus/{codigo}", method = RequestMethod.GET)
    public @ResponseBody
    List<Menu> buscar(@PathVariable(value = "codigo") String codigoCasa) {
        return menuService.buscar(codigoCasa);
    }
}
