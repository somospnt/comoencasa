package comoencasa.controller;

import comoencasa.domain.Pedido;
import comoencasa.domain.Usuario;
import comoencasa.service.ClienteService;
import comoencasa.service.PedidoService;
import comoencasa.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class PedidoRestController {

    @Autowired
    private PedidoService pedidoService;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private UsuarioService usuarioService;

    @RequestMapping(value = "/api/pedidos", method = RequestMethod.POST)
    public @ResponseBody Pedido guardar(@RequestBody Pedido pedido) {
        Usuario usuarioGuardado = usuarioService.guardar(pedido.getUsuario());
        clienteService.generarCliente(usuarioGuardado, pedido.getIdCasa());
        pedido.setUsuario(usuarioGuardado);

        return pedidoService.guardar(pedido);
    }

}
