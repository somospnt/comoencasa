package comoencasa.controller;

import comoencasa.domain.Casa;
import comoencasa.service.CasaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CasaRestController {

    @Autowired
    private CasaService casaService;

    @RequestMapping(value = "/api/casas", method = RequestMethod.GET)
    public @ResponseBody
    List<Casa> buscar() {
        return casaService.buscar();
    }

    @RequestMapping(value = "/api/casas/{codigo}", method = RequestMethod.GET)
    public @ResponseBody
    Casa buscar(@PathVariable(value = "codigo") String codigoCasa) {
        return casaService.buscar(codigoCasa);
    }
}
