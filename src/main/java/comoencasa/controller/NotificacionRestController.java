package comoencasa.controller;

import comoencasa.domain.Casa;
import comoencasa.service.CasaService;
import comoencasa.service.NotificacionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NotificacionRestController {

    @Autowired
    private NotificacionService notificacionService;

    @Autowired
    private CasaService casaService;

    @RequestMapping(value = "/api/notificaciones/pedidos/{codigoCasa}", method = RequestMethod.PUT)
    public @ResponseBody
    String notificarPedidosPendientes(@PathVariable(value = "codigoCasa") String codigoCasa) {
        Casa casa = casaService.buscar(codigoCasa);
        notificacionService.pedidosPendientes(casa);
        return "Email enviado";
    }

}
