package comoencasa.controller;

import comoencasa.domain.Pedido;
import comoencasa.domain.Usuario;
import comoencasa.service.ClienteService;
import comoencasa.service.PedidoService;
import comoencasa.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class UsuarioRestController {

    @Autowired
    private PedidoService pedidoService;

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private UsuarioService usuarioService;

    @RequestMapping(value = "/api/usuarios/pedido", method = RequestMethod.POST)
    public @ResponseBody
    Pedido guardar(@RequestBody Pedido pedido) {
        Usuario usuarioValidado = usuarioService.validarUsuario(pedido.getUsuario());
        clienteService.generarCliente(usuarioValidado, pedido.getIdCasa());
        pedido.setUsuario(usuarioValidado);
        return pedidoService.guardar(pedido);
    }
}
