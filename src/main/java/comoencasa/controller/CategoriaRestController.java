package comoencasa.controller;

import comoencasa.domain.Categoria;
import comoencasa.service.CategoriaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CategoriaRestController {

    @Autowired
    private CategoriaService categoriaService;

    @RequestMapping(value = "/api/categorias", method = RequestMethod.GET)
    public @ResponseBody
    List<Categoria> buscar() {
        return categoriaService.buscar();
    }
}
