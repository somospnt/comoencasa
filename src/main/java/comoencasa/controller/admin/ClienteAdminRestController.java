package comoencasa.controller.admin;

import comoencasa.domain.Cliente;
import comoencasa.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ClienteAdminRestController {

    @Autowired
    private ClienteService clienteService;


    @RequestMapping(value = "/api/admin/clientes", method = RequestMethod.GET)
    public @ResponseBody
    List<Cliente> buscar() {
        return clienteService.buscar();
    }

    @RequestMapping(value = "/api/admin/clientes/notas", method = RequestMethod.PUT)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void actualizarNotas(@RequestBody Cliente cliente) {
        clienteService.actualizarNotas(cliente);
    }
}
