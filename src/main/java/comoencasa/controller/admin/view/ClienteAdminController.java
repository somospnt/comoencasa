package comoencasa.controller.admin.view;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class ClienteAdminController {
    
        
    @RequestMapping("admin/partials/clientes.htm")
    public String clientes(Model model) {        
        return "admin-partials-clientes";
    }
}
