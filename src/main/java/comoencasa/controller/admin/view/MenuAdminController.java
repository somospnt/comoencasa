package comoencasa.controller.admin.view;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MenuAdminController {
    
        
    @RequestMapping("/admin/partials/menus.htm")
    public String dashboard(Model model) {        
        return "admin-partials-menus";
    }
}
