package comoencasa.controller.admin.view;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PedidoAdminController {
    
     
    @RequestMapping("/admin/partials/pedidos.htm")
    public String pedidos(Model model) {        
        return "admin-partials-pedidos";
    }
}
