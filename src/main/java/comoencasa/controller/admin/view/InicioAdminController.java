package comoencasa.controller.admin.view;

import comoencasa.domain.Usuario;
import comoencasa.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class InicioAdminController {
    
    @Autowired
    private UsuarioService usuarioService;
    
    @RequestMapping("/admin/")
    public String admin(Model model) {
        return "redirect:/admin/inicio.htm";
    }
    
    @RequestMapping("/admin/index.html")
    public String index(Model model) {
        return "redirect:/admin/inicio.htm";
    }
    
    @RequestMapping("/admin/inicio.htm")
    public String inicio(Model model) {        
        Usuario usuario = usuarioService.obtenerUsuarioLogueado();
        model.addAttribute("casa", usuario.getCasa());
        return "admin-inicio";
    }
    
    @RequestMapping("/admin/partials/inicio.htm")
    public String inicioPartial(Model model) {
        return "admin-partials-inicio";
    }
}
