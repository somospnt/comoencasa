package comoencasa.controller.admin;

import comoencasa.domain.Categoria;
import comoencasa.domain.Menu;
import comoencasa.domain.Usuario;
import comoencasa.service.CategoriaService;
import comoencasa.service.MenuService;
import comoencasa.service.UsuarioService;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
public class MenuAdminRestController {

    private final static Logger logger = LoggerFactory.getLogger(MenuAdminRestController.class);

    @Autowired
    private MenuService menuService;

    @Autowired
    private UsuarioService usuarioService;

    @Autowired
    private CategoriaService categoriaService;

    @RequestMapping(value = "/api/admin/menus", method = RequestMethod.GET)
    public @ResponseBody
    List<Menu> buscar() {
        Usuario usuario = usuarioService.obtenerUsuarioLogueado();
        return menuService.buscar(usuario.getCasa().getCodigo());
    }

    @RequestMapping(value = "/api/admin/menus", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    public @ResponseBody
    Menu crear(@RequestParam("nombre") String nombre,
            @RequestParam("descripcion") String descripcion,
            @RequestParam("categoria") String categoriaId,
            @RequestParam(value = "visible", required = false) boolean visible,
            @RequestParam(value = "archivo", required = false) MultipartFile archivo) {

        Usuario usuario = usuarioService.obtenerUsuarioLogueado();
        Categoria categoria = null;
        InputStream inputStream = null;
        Menu menu = new Menu();
        menu.setCasa(usuario.getCasa());
        menu.setNombre(nombre);
        menu.setDescripcion(descripcion);
        menu.setVisible(visible);

        if (!categoriaId.equals("")) {
            categoria = categoriaService.buscar(Long.parseLong(categoriaId));
        }

        menu.setCategoria(categoria);

        if (archivo != null && !archivo.isEmpty()) {
            try {
                inputStream = archivo.getInputStream();
                menu.setImagen(archivo.getOriginalFilename());
            } catch (IOException ex) {
                logger.warn("No se pudo subir el archivo", ex);
            }
        }
        return menuService.guardar(menu, inputStream);
    }

    @RequestMapping(value = "/api/admin/menus/{idMenu}", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void actualizar(
            @PathVariable Long idMenu, @RequestParam(value = "nombre") String nombre, @RequestParam(value = "descripcion") String descripcion,
            @RequestParam("categoria") String categoriaId,
            @RequestParam(value = "imagen", required = false) String imagen,
            @RequestParam(value = "visible", required = false) boolean visible,
            @RequestParam(value = "archivo", required = false) MultipartFile archivo) {
        Usuario usuario = usuarioService.obtenerUsuarioLogueado();
        InputStream inputStream = null;
        Menu menu = new Menu();
        menu.setId(idMenu);
        menu.setCasa(usuario.getCasa());
        menu.setNombre(nombre);
        menu.setDescripcion(descripcion);
        menu.setImagen(imagen);
        menu.setVisible(visible);
        if (archivo != null && !archivo.isEmpty()) {
            try {
                inputStream = archivo.getInputStream();
            } catch (IOException ex) {
                logger.warn("No se pudo subir el archivo", ex);
            }
        }
        Categoria categoria = null;
        if (!categoriaId.equals("")) {
            categoria = categoriaService.buscar(Long.parseLong(categoriaId));
        }

        menu.setCategoria(categoria);
        menuService.actualizar(menu, inputStream);
    }

    @RequestMapping(value = "/api/admin/menus/{idMenu}", method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void borrar(@PathVariable Long idMenu) {
        menuService.borrar(idMenu);
    }
}
