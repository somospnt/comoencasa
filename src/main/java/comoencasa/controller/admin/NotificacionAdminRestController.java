package comoencasa.controller.admin;

import comoencasa.domain.Usuario;
import comoencasa.service.NotificacionService;
import comoencasa.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NotificacionAdminRestController {

    @Autowired
    private NotificacionService notificacionService;

    @Autowired
    private UsuarioService usuarioService;

    @RequestMapping(value = "/api/admin/notificaciones/pedidos", method = RequestMethod.PUT)
    public @ResponseBody
    String notificarPedidosPendientes(String codigoCasa) {
        Usuario usuario = usuarioService.obtenerUsuarioLogueado();        
        notificacionService.pedidosPendientes(usuario.getCasa());
        return "Email enviado";
    }

}
