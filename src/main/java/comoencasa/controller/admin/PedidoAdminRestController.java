package comoencasa.controller.admin;

import comoencasa.domain.EstadoPedido;
import comoencasa.domain.Pedido;
import comoencasa.domain.Usuario;
import comoencasa.service.PedidoService;

import comoencasa.service.UsuarioService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
public class PedidoAdminRestController {

    @Autowired
    private PedidoService pedidoService;

    @Autowired
    private UsuarioService usuarioService;
    
    @RequestMapping(value = "/api/admin/pedidos", method = RequestMethod.GET)
    public @ResponseBody List<Pedido> buscarOrdenadoPorFechaDesc(@RequestParam(value = "estado", required=false) EstadoPedido estado) {        
        Usuario usuario = usuarioService.obtenerUsuarioLogueado();
        if (estado != null) {
            return pedidoService.buscarPorEstadoOrdenadoPorFechaDesc(usuario.getCasa().getId(), estado);
        } else {
            return pedidoService.buscarOrdenadoPorFechaDesc(usuario.getCasa().getId());
        }
    }
    
}