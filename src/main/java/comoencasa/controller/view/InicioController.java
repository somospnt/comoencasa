package comoencasa.controller.view;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class InicioController {
        
    @RequestMapping("/index.html")
    public String inicio(Model model) {        
        return "front-inicio";
    }
    
    @RequestMapping("/")
    public String admin(Model model) {
        return "redirect:/index.html";
    }
}
