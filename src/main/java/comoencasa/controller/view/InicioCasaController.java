package comoencasa.controller.view;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class InicioCasaController {
        
    private static final String TEMA_DEFAULT = "heroic";
    
    @RequestMapping("/{codigoCasa}")
    public String inicio(@PathVariable(value = "codigoCasa") String codigoCasa, Model model) {        
        model.addAttribute("codigoCasa", codigoCasa);
        model.addAttribute("tema", TEMA_DEFAULT);
        return "casa-inicio";
    }
}
