package comoencasa.config;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource datasource;
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void configure(WebSecurity web) throws Exception {
        web.ignoring()
                .antMatchers("/styles/**")
                .antMatchers("/scripts/**")
                .antMatchers("/images/**")
                .antMatchers("/fonts/**")
                .antMatchers("/font-awesome-4.1.0/**")
                .antMatchers("/admin/static/**")
                .antMatchers("/**/favicon.ico");
                
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.rememberMe().key("comoencasa-remember-me").tokenValiditySeconds(2592000); //30 dias
        http.formLogin().loginPage("/login.htm").loginProcessingUrl("/login");

        http.authorizeRequests()
                .antMatchers("/admin/").hasRole("CASA")
                .antMatchers("/admin/*.htm").hasRole("CASA")
                .antMatchers("/admin/**/*.htm").hasRole("CASA")
                .antMatchers("/api/admin/**").hasRole("CASA")
                .anyRequest().permitAll();
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .dataSource(datasource)
                .usersByUsernameQuery("select mail, password, enabled from cec_usuario where mail = ?")
                .authoritiesByUsernameQuery("select u.mail, r.rol from cec_usuario u, cec_rol r where u.id = r.id_usuario and u.mail = ?")
                .passwordEncoder(bCryptPasswordEncoder);
    }

}
