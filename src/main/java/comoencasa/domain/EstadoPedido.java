package comoencasa.domain;

public enum EstadoPedido {

    PENDIENTE,
    INFORMADO
}
