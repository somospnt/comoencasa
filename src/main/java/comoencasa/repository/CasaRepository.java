package comoencasa.repository;

import comoencasa.domain.Casa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CasaRepository extends JpaRepository<Casa, Long> {

    Casa findOneByCodigo(String codigo);
}
