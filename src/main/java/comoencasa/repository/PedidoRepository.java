package comoencasa.repository;

import comoencasa.domain.EstadoPedido;
import comoencasa.domain.Pedido;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PedidoRepository extends JpaRepository<Pedido, Long> {

    List<Pedido> findByIdCasaAndEstado(Long idCasa, EstadoPedido estado);
    
    List<Pedido> findByIdCasaOrderByFechaDesc(Long idCasa);    
    
    List<Pedido> findByIdCasaAndEstadoOrderByFechaDesc(Long idCasa, EstadoPedido estadoPedido);    
}
