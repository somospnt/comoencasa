package comoencasa.repository;

import comoencasa.domain.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {
    Cliente findByUsuarioIdAndIdCasa(Long id, Long idCasa);

    List<Cliente> findByIdCasa(Long idCasa);
}
