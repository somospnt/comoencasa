
package comoencasa.service;

        import comoencasa.domain.Usuario;

public interface UsuarioService {

    Usuario obtenerUsuarioLogueado();

    Usuario validarUsuario(Usuario usuario);

    Usuario guardar(Usuario usuario);
}
