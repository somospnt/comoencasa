package comoencasa.service;

import comoencasa.domain.Casa;
import comoencasa.domain.Pedido;

public interface NotificacionService {

    void pedidosPendientes(Casa casa);

    public void pedido(Pedido pedido);
}
