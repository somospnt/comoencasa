package comoencasa.service;

import comoencasa.domain.Categoria;
import java.util.List;

public interface CategoriaService {

    List<Categoria> buscar();

    Categoria buscar(Long id);
}
