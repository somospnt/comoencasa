package comoencasa.service;

import comoencasa.domain.Casa;
import java.util.List;

public interface CasaService {

    List<Casa> buscar();

    Casa buscar(String codigoCasa);
}
