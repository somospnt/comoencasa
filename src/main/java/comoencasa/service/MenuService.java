package comoencasa.service;

import comoencasa.domain.Menu;
import java.io.InputStream;
import java.util.List;

public interface MenuService {

    List<Menu> buscar(String codigoCasa);

    public Menu guardar(Menu menu, InputStream inputStream);

    public Menu actualizar(Menu menu, InputStream inputStream);

    public void borrar(Long idMenu);
    
}
