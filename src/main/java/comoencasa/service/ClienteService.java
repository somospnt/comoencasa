package comoencasa.service;

import comoencasa.domain.Cliente;
import comoencasa.domain.Usuario;

import java.util.List;

public interface ClienteService {

    void generarCliente(Usuario usuario, Long idCasa);

    List<Cliente> buscar();

    void actualizarNotas(Cliente cliente);
}
