package comoencasa.service.impl;

import comoencasa.domain.Categoria;
import comoencasa.repository.CategoriaRepository;
import comoencasa.service.CategoriaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoriaServiceImpl implements CategoriaService {

    @Autowired
    private CategoriaRepository categoriaRepository;

    @Override
    public Categoria buscar(Long id) {
        return categoriaRepository.findOne(id);
    }

    @Override
    public List<Categoria> buscar() {
        return categoriaRepository.findAll();
    }

}
