package comoencasa.service.impl;

import comoencasa.domain.Casa;
import comoencasa.domain.EstadoPedido;
import comoencasa.domain.Pedido;
import comoencasa.repository.CasaRepository;
import comoencasa.repository.PedidoRepository;
import comoencasa.service.NotificacionService;

import java.io.StringWriter;
import java.util.List;
import javax.annotation.Resource;
import javax.mail.internet.MimeMessage;
import javax.transaction.Transactional;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;

/**
 *
 * @author smurua.
 */
@Service
public class NotificacionServiceImpl implements NotificacionService {

    @Autowired
    PedidoRepository pedidoRepository;

    @Autowired
    CasaRepository casaRepository;

    Logger log = LoggerFactory.getLogger(NotificacionServiceImpl.class);

    @Resource
    private JavaMailSender emailSender;

    @Override
    @Transactional
    public void pedidosPendientes(Casa casa) {
        log.debug("[pedidosPendientes] Entre a generar el pedido de  comanda");
        List<Pedido> pedidosPendientes = pedidoRepository.findByIdCasaAndEstado(casa.getId(), EstadoPedido.PENDIENTE);
        String asunto = "Resumen de pedidos - " + casa.getNombre();

        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        velocityEngine.init();

        Template template = velocityEngine.getTemplate("templates/email-comanda-template.vm");
        VelocityContext velocityContext = new VelocityContext();

        velocityContext.put("pedidosPendientes", pedidosPendientes);
        velocityContext.put("nombreCasa", casa.getNombre());

        StringWriter stringWriter = new StringWriter();
        template.merge(velocityContext, stringWriter);

        String mensaje = stringWriter.toString();
        log.debug("[pedidosPendientes] Voy a enviar el siguiente Mensaje: " + mensaje);


        MimeMessagePreparator preparator = new MimeMessagePreparator() {
            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage, MimeMessageHelper.MULTIPART_MODE_MIXED);
                message.setTo(casa.getMail());
                message.setFrom("info@comoencasa.de");
                message.setValidateAddresses(false);
                message.setText(mensaje, true);
                message.setSubject(asunto);
            }
        };
        log.debug("[pedidosPendientes] Voy a enviar el mail");
        emailSender.send(preparator);

        log.debug("[pedidosPendientes] Mandé el email y voy a actualizar los estados");
        for (Pedido pedido : pedidosPendientes) {
            pedido.setEstado(EstadoPedido.INFORMADO);
        }
    }

    @Override
    public void pedido(Pedido pedido) {        
        Casa casa = casaRepository.findOne(pedido.getIdCasa());


        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        velocityEngine.init();

        Template template = velocityEngine.getTemplate("templates/email-pedido-template.vm");
        VelocityContext velocityContext = new VelocityContext();

        velocityContext.put("pedido", pedido);
        velocityContext.put("casa", casa);

        StringWriter stringWriter = new StringWriter();
        template.merge(velocityContext, stringWriter);

        String mensaje = stringWriter.toString();
        log.debug("[pedido] Voy a enviar el siguiente Mensaje: " + mensaje);
        
        MimeMessagePreparator preparator = new MimeMessagePreparator() {
            @Override
            public void prepare(MimeMessage mimeMessage) throws Exception {
                MimeMessageHelper message = new MimeMessageHelper(mimeMessage, MimeMessageHelper.MULTIPART_MODE_MIXED);
                message.setTo(pedido.getUsuario().getMail());
                message.setFrom(casa.getMail());
                message.setValidateAddresses(false);
                message.setText(mensaje, true);
                message.setSubject("Pedido realizado en " + casa.getNombre());
            }
        };
        emailSender.send(preparator);        
    }

}
