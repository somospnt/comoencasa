package comoencasa.service.impl;

import comoencasa.domain.Casa;
import comoencasa.domain.Menu;
import comoencasa.domain.Usuario;
import comoencasa.repository.MenuRepository;
import comoencasa.service.MenuService;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import comoencasa.service.UsuarioService;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class MenuServiceImpl implements MenuService {

    private final static Logger logger = LoggerFactory.getLogger(MenuServiceImpl.class);

    private static final int IMG_WIDTH = 640;
    private static final int IMG_HEIGHT = 480;

    @Value("${comoencasa.casa.directorio}")
    private String pathDirectorioCasa;

    @Value("${comoencasa.casa.archivos.imagen.menu}")
    private String pathImagenMenu;

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Override
    public List<Menu> buscar(String codigoCasa) {
        return menuRepository.findByCasaCodigo(codigoCasa);
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_CASA')")
    public Menu guardar(Menu menu, InputStream inputStream) {
        if (inputStream == null) {
            menu.setImagen(null);
        } else {
            guardarImagen(menu, inputStream);
        }
        menuRepository.save(menu);
        return menu;
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_CASA')")
    public Menu actualizar(Menu menu, InputStream inputStream) {
        Menu menuOriginal = menuRepository.findOne(menu.getId());
        String nombreArchivo = menu.getImagen();

        if (nombreArchivo == null && inputStream == null) {
            borrarImagen(menuOriginal);
        } else if (inputStream != null) {
            borrarImagen(menuOriginal);
            guardarImagen(menu, inputStream);
        } else {
            nombreArchivo = menuOriginal.getImagen();
        }
        BeanUtils.copyProperties(menu, menuOriginal);
        menuOriginal.setImagen(nombreArchivo);
        return menuOriginal;
    }

    @Override
    @Transactional
    @PreAuthorize("hasRole('ROLE_CASA')")
    public void borrar(Long idMenu) {
        Usuario usuario = usuarioService.obtenerUsuarioLogueado();
        Menu menuOriginal = menuRepository.findOne(idMenu);
        if (usuario.getCasa().getId().equals(menuOriginal.getCasa().getId())) {
            borrarImagen(menuOriginal);
            menuRepository.delete(menuOriginal);
        } else {
            logger.error("El usuario [" + usuario.getMail() + "] intentÃ³ borrar el menÃº de id [" + menuOriginal.getId() + "] que no le pertence");
            throw new AccessDeniedException("El menÃº que estÃ¡ intentando borrar no le pertenece al usuario logueado");
        }
    }

    private void guardarImagen(Menu menu, InputStream inputStream) {
        Casa casa = menu.getCasa();
        File archivoImagenMenu = obtenerArchivoImagenDelMenu(casa.getCodigo(), menu.getImagen());

        try {
            FileUtils.copyInputStreamToFile(inputStream, archivoImagenMenu);
            
            BufferedImage originalImage = ImageIO.read(archivoImagenMenu);
            int type = originalImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : originalImage.getType();
            BufferedImage resizeImage = resizeImage(originalImage, type);
            ImageIO.write(resizeImage, "jpg", archivoImagenMenu);
        } catch (IOException ex) {
            logger.error("Error al copiar el archivo", ex);
            menu.setImagen(null);
        }
    }

    private static BufferedImage resizeImage(BufferedImage originalImage, int type){
        int originalWidth = originalImage.getWidth();
        int originalHeight = originalImage.getHeight();
        double ratio =  (double) IMG_WIDTH / originalWidth;
        double calculatedHeight = originalHeight * ratio;
	BufferedImage resizedImage = new BufferedImage(IMG_WIDTH, (int) calculatedHeight, type);
	Graphics2D g = resizedImage.createGraphics();
	g.drawImage(originalImage, 0, 0, IMG_WIDTH, (int) calculatedHeight, null);
	g.dispose();
 
	return resizedImage;
    }
    
    private void borrarImagen(Menu menu) {
        Casa casa = menu.getCasa();
        File archivoImagenMenu = obtenerArchivoImagenDelMenu(casa.getCodigo(), menu.getImagen());
        FileUtils.deleteQuietly(archivoImagenMenu);
    }

    private File obtenerArchivoImagenDelMenu(String codigoCasa, String nombreImagen) {
        StringBuilder rutaImagen = new StringBuilder();
        rutaImagen.append(pathDirectorioCasa).append("/").append(codigoCasa).append("/").append(pathImagenMenu).append("/").append(nombreImagen);
        File imagen = new File(rutaImagen.toString());
        return imagen;
    }
}
