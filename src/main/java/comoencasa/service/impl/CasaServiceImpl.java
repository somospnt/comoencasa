package comoencasa.service.impl;

import comoencasa.domain.Casa;
import comoencasa.repository.CasaRepository;
import comoencasa.service.CasaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CasaServiceImpl implements CasaService {

    @Autowired
    private CasaRepository casaRepository;

    @Override
    public List<Casa> buscar() {
        return casaRepository.findAll();
    }

    @Override
    public Casa buscar(String codigoCasa) {
        return casaRepository.findOneByCodigo(codigoCasa);
    }

}
