package comoencasa.service.impl;

import comoencasa.domain.Usuario;
import comoencasa.repository.UsuarioRepository;
import comoencasa.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    @Override
    @PreAuthorize("isAuthenticated()")
    public Usuario obtenerUsuarioLogueado() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return usuarioRepository.findByMail(auth.getName());
    }

    @Transactional
    public Usuario validarUsuario(Usuario usuario) {
        Usuario usuarioBase = usuarioRepository.findByMail(usuario.getMail());

        if (usuarioBase == null) {
            throw new UsernameNotFoundException("El mail ingresado no se encuentra registrado");
        }

        if (!bCryptPasswordEncoder.matches(usuario.getPassword(),usuarioBase.getPassword())) {
            throw new BadCredentialsException("La clave ingresada no coincide");
        }

        return usuarioBase;
    }

    @Override
    public Usuario guardar(Usuario usuario) {
        usuario.setPassword(bCryptPasswordEncoder.encode(usuario.getPassword()));
        return usuarioRepository.save(usuario);
    }

}
