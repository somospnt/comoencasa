package comoencasa.service.impl;

import comoencasa.domain.*;
import comoencasa.repository.MenuRepository;
import comoencasa.repository.PedidoRepository;
import comoencasa.repository.UsuarioRepository;
import comoencasa.service.NotificacionService;
import comoencasa.service.PedidoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;

@Service
public class PedidoServiceImpl implements PedidoService {
    
    private final static Logger logger = LoggerFactory.getLogger(PedidoServiceImpl.class);
    @Autowired
    private PedidoRepository pedidoRepository;
    
    @Autowired
    private MenuRepository menuRepository;
        
    @Autowired
    private NotificacionService notificacionService;
    
    @Override
    @Transactional
    public Pedido guardar(Pedido pedido) {
        pedido.setEstado(EstadoPedido.PENDIENTE);
        pedido.setFecha(new Date());
        for (PedidoItem pedidoItem : pedido.getItems()) {
            Menu menu = menuRepository.findOne(pedidoItem.getMenu().getId());
            pedidoItem.setMenu(menu);
            pedidoItem.setPedido(pedido);
        }
        Pedido pedidoGuardado = pedidoRepository.save(pedido);                
        try {
            notificacionService.pedido(pedidoGuardado);
        } catch (Exception ex){
            logger.warn("Error al enviar el mail para  [" + pedido.getUsuario().getNombre()+ "] con mail [" + pedido.getUsuario().getMail() + "]", ex);
        }
        return pedidoGuardado;
    }

    @Override
    @PreAuthorize("hasRole('ROLE_CASA')")
    public List<Pedido> buscarOrdenadoPorFechaDesc(Long idCasa) {
       return pedidoRepository.findByIdCasaOrderByFechaDesc(idCasa);
    }

    @Override
    @PreAuthorize("hasRole('ROLE_CASA')")
    public List<Pedido> buscarPorEstadoOrdenadoPorFechaDesc(Long idCasa, EstadoPedido estado) {
        return pedidoRepository.findByIdCasaAndEstadoOrderByFechaDesc(idCasa, estado);
    }
    
}
