package comoencasa.service.impl;

import comoencasa.domain.Cliente;
import comoencasa.domain.Usuario;
import comoencasa.repository.ClienteRepository;
import comoencasa.service.ClienteService;
import comoencasa.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
@Transactional
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    @Autowired
    private UsuarioService usuarioService;

    @Override
    public void generarCliente(Usuario usuario, Long idCasa) {
        if (usuario == null || idCasa == null) {
            throw new IllegalArgumentException(
                    "Alguno de los parametros para crear el usuario son inválidos Usuario[" + usuario + "] idCasa [" + idCasa + "]");
        }

        Cliente cliente = clienteRepository.findByUsuarioIdAndIdCasa(usuario.getId(), idCasa);

        if (cliente == null) {
            cliente = new Cliente();
            cliente.setFechaAlta(new Date());
            cliente.setUsuario(usuario);
            cliente.setIdCasa(idCasa);
            clienteRepository.save(cliente);
        }
    }

    @Override
    @PreAuthorize("hasRole('ROLE_CASA')")
    public List<Cliente> buscar() {
        Usuario usuario = usuarioService.obtenerUsuarioLogueado();
        return clienteRepository.findByIdCasa(usuario.getCasa().getId());
    }

    @Override
    @PreAuthorize("hasRole('ROLE_CASA')")
    public void actualizarNotas(Cliente cliente) {
        Usuario usuario = usuarioService.obtenerUsuarioLogueado();
        Cliente clienteBase = clienteRepository.findOne(cliente.getId());
        if (clienteBase.getIdCasa() != usuario.getCasa().getId()) {
            throw new AccessDeniedException("No puede modificar un clientes ajeno");
        }
        clienteBase.setNotas(cliente.getNotas());
        clienteRepository.save(clienteBase);
    }
}
