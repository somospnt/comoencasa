package comoencasa.service;

import comoencasa.domain.EstadoPedido;
import comoencasa.domain.Pedido;
import java.util.List;

public interface PedidoService {

    Pedido guardar(Pedido pedido);

    List<Pedido> buscarOrdenadoPorFechaDesc(Long idCasa);
    
    List<Pedido> buscarPorEstadoOrdenadoPorFechaDesc(Long idCasa, EstadoPedido estado);
}
