<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<div ng-controller="InicioCtrl" ng-init="init('<c:out value="${codigoCasa}"/>')">
    <header id="header" class="full-bg" style="background-image: url('casas/{{codigoCasa}}/background.jpg')">
        <div class="full-bg-overlay black-pat text-center">
            <h2 class="animated fadeInUp visible" data-animation="fadeInUp" data-animation-delay="1000">{{casa.nombre}}</h2>
            <h3>{{casa.descripcion}}</h3>
        </div>
    </header>

    <nav id="navigation">
        <div class="nav-container">	
            <ul>
                <li><a class="active" href="#nuestros-menus"><span class="extrabold">Nuestros menús</span></a></li>
                <li><a href="#contactanos"><span class="extrabold">Contactanos</span></a></li>
                <li><a href="#carrito"><span class="extrabold highlight"><i class="fa fa-shopping-cart fa-lg"></i> ({{cantidadViandas()}})</span></a></li>
            </ul>			
        </div>
    </nav>

    <nav id="mobile-navigation">
        <div class="mobile-nav-container">
            <div id="menu-toggle">
                <i class="fa fa-bars"></i>
            </div>		
            <ul class="inactive">
                <li><a class="active" href="#nuestros-menus"><span class="extrabold">Nuestros menús</span></a></li>
                <li><a href="#contactanos"><span class="extrabold">Contactanos</span></a></li>
                <li><a href="#carrito"><span class="extrabold highlight"><i class="fa fa-shopping-cart fa-lg"></i></span></a></li>
            </ul>			
        </div>
    </nav>
    <a href="#" class="scrollup">
        <i class="fa fa-angle-double-up"></i>
    </a><!-- end .scrollup -->
    <section id="nuestros-menus" class="big-menu section"  ng-controller="ViandaCtrl">
        <!-- <img id="food-logo" src="images/food_logo.png" alt=""> -->
        <div class="container">
            <div class="row">
                <div class="section-title animated" data-animation="fadeInUp" data-animation-delay="700">
                    <h2><span class="extrabold">Nuestros</span> Menús</h2>
                    <div class="section-title-line">
                        <div class="section-title-icon">
                            <img src="images/logo-mini-cb.png" alt="">
                        </div>
                        <hr>
                    </div>
<!--                    <p>
                        Proponemos una alimentación saludable aportando todos los nutrientes esenciales y la energía que cada persona necesita para mantenerse sana.
                    </p>	-->
                </div>
                <div class="special-tab">		

                    <ul class="cb-special-tabs-titles" >
                        <li ng-repeat="categoria in categorias| filter: {empty: false}"
                            ng-click="cambiarCategoria(categoria)" ng-class='{current:$first, current:esCategoriaSelecccionada(categoria)}'>
                            <div class="food-icon-{{categoria.nombre}}"></div><span>{{categoria.nombre}}</span>
                        </li>                        
                    </ul>                    
                    <!--
                    <ul class="special-tabs-titles"  cb-tabs cb-tabs-content=".special-tab-content">
                        <li ng-repeat="categoria in categorias| filter: {empty: false}"
                            ng-click="cambiarCategoria(categoria)">
                            <div class="food-icon-{{categoria.nombre}}"></div><span>{{categoria.nombre}}</span>
                        </li>                        
                    </ul>
                    -->
                    <div class="special-tab-content">
                        <div class="row">
                            <div class="col-md-4 col-sm-6 col-xs-6"  ng-repeat="vianda in viandas | filter : filtroCategoria">
                                <div class="food-menu-item animated visible" data-animation="fadeInUp" data-animation-delay="1000">
                                    <div class="pull-left food-menu-item-img-outer" ng-click="mostrarVianda(vianda)">
                                        <img class="img-circle" ng-if="vianda.imagen" ng-show="vianda.imagen" ng-src="casas/{{codigoCasa}}/img/viandas/{{vianda.imagen}}" alt="{{vianda.nombre}}"/>
                                        <img class="img-circle" ng-if="vianda.imagen == null" src="temas/heroic/img/sin-imagen.png" alt="{{vianda.nombre}}"/>
                                    </div>
                                    <div class="food-menu-desc">
                                        <h5>{{vianda.nombre}}</h5>                                        
                                        <!--<div>
                                            {{vianda.descripcion}}
                                        </div>-->
                                        <div class="food-menu-details cb-carrito-form form-inline">
                                            <input type="text" class="form-control" size="2" maxlength="2" placeholder="Cant" ng-model="vianda.cantidad"/>
                                            <span class="cb-button button"><button ng-click="agregarAlCarrito(vianda)"><i class="fa fa-shopping-cart fa-lg"></i></button></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>																		
                    </div>	
                </div>			
            </div>
        </div>
    </section>
    <section id="carrito" class="section grey section-without-pb" ng-controller='CarritoCtrl'>
        <div class="container-fluid">
            <div class="row">
                <div class="container">
                    <div class="section-title animated fadeInUp visible" data-animation="fadeInUp" data-animation-delay="">
                        <h2>Carrito</h2>
                        <div class="section-title-line" style="width: 267px;">	
                            <div class="section-title-icon cb-resaltar">
                                <i class="fa fa-shopping-cart fa-3x"></i> <strong>({{cantidadViandas()}})</strong>
                            </div>
                            <hr>                            
                        </div>  
                        <p>Realizá tu pedido online</p>
                    </div><!-- end .section-title -->
                    <div class="col-md-12 side-image-right pull-right">
                        <div class="alert alert-info" role="alert" ng-hide="tieneViandas()">
                            <strong>¡Agregá tu pedido!</strong> Elegí los productos y las cantidades y agregalos al carrito.
                        </div>
                        <div ng-show="tieneViandas()">   
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Cantidad</th>
                                    <th>Descripción</th>
                                    <th></th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr ng-repeat="item in carrito.viandas">
                                        <td>{{item.cantidad}}</td>
                                        <td>{{item.nombre}}</td>
                                        <td>
                                            <span class="cb-eliminar-button" ng-click="eliminiarDelCarrito(item)"><i class="fa fa-times-circle fa-2x"></i></span>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <span class="cb-button cb-button-pedir button pull-right">
                                <button type="button" class="button" ng-click="abrirFormularioPedido()"><i class="fa fa-check"></i> Pedir</button>
                            </span>
                        </div> 
                    </div>                        		 	
                </div>
            </div>
        </div>
    </section>

    <section id="contactanos" class="contact section section-without-pb">
        <div class="container-fluid">
            <div class="row">
                <div class="section-title animated" data-animation="fadeInUp" data-animation-delay="700">
                    <h2><span class="extrabold">Contactanos</span></h2>
                    <div class="section-title-line">
                        <div class="section-title-icon">
                            <img src="images/logo-mini-cb.png" alt="">
                        </div>
                        <hr>
                    </div>				
                    <p>
                        Escribinos a <a href="mailto:{{casa.mail}}">{{casa.mail}}</a> y nos pondremos en contacto a la brevedad.
                    </p>	
                </div>
                <div class="col-md-6 side-image-right pull-right">
                    <img src="/casas/{{codigoCasa}}/{{casa.imagen}}" alt="{{casa.nombre}}">
                </div>
                <div class="col-md-6">
                    <ul class="contact-details">
                        <!--<li class="contact-address">Tu dirección 222, Ciudad Autónoma de Buenos Aires</li>-->
                        <!--<li class="contact-tel">+ 54 11 5555-5555</li>-->
                        <li class="contact-email">{{casa.mail}}</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

</div>
    
<script type="text/ng-template" id="partials/viandaModal.html">
    <div class="modal-header">
        <button type="button" class="close" aria-hidden="true" ng-click="cerrar()"> &times;</button>
        <h3 class="modal-title">{{vianda.nombre}}</h3>
    </div>    
    <div class="modal-body">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div>
                    <img class="img-circle" ng-if="vianda.imagen" ng-show="vianda.imagen" ng-src="casas/{{codigoCasa}}/img/viandas/{{vianda.imagen}}" alt="{{vianda.nombre}}" />
                    <img class="img-circle" ng-if="vianda.imagen == null" src="temas/heroic/img/sin-imagen.png" alt="{{vianda.nombre}}" />
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div ng-bind-html="vianda.descripcion"></div>
            </div>
        </div>
    </div>
</script>

<script type="text/ng-template" id="partials/pedidoModal.html">
    <div class="modal-header">
        <h3 class="modal-title">Complete sus datos</h3>
    </div>
    <div class="modal-body">
        <div class="alert" ng-class="mensaje.tipo === 'error' ? ' alert-danger' : ' alert-success'" ng-show="tieneMensaje()">
            {{mensaje.descripcion}}
        </div>
        <div class="row">
            <div class="col-sm-12">
                <tabset justified="true" type="pills">
                    <tab select="cambiarUsuarioExiste(false)">
                        <tab-heading>
                            <i class='fa fa-asterisk'></i> Primer pedido
                        </tab-heading>
                        <br>
                        <form role="form" name="pedidoForm">
                            <div class="form-group" ng-class="{ 'has-error' : pedidoForm.nombre.$invalid && !pedidoForm.nombre.$pristine }">
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" ng-model="usuario.nombre" required>
                                <p ng-show="pedidoForm.nombre.$invalid && !pedidoForm.nombre.$pristine" class="help-block">El nombre es requerido.</p>
                            </div>
                            <div class="form-group" ng-class="{ 'has-error' : pedidoForm.mail.$invalid && !pedidoForm.mail.$pristine }">
                                <input type="email" class="form-control" id="email" name="mail" placeholder="Email" ng-model="usuario.mail" required>
                                <p ng-show="pedidoForm.mail.$invalid && !pedidoForm.mail.$pristine" class="help-block">El email es requerido.</p>
                            </div>
                            <div class="form-group" ng-class="{ 'has-error' : pedidoForm.clave.$invalid && !pedidoForm.clave.$pristine }">
                                <input type="password" class="form-control" id="clave" name="clave" placeholder="Clave" ng-model="usuario.clave" required>
                                <p ng-show="pedidoForm.clave.$invalid && !pedidoForm.clave.$pristine" class="help-block">La clave es requerida.</p>
                            </div>
                            <div class="form-group" ng-class="{ 'has-error' : pedidoForm.repetirClave.$invalid && !pedidoForm.repetirClave.$pristine }">
                                <input type="password" class="form-control" id="repetirClave" name="repetirClave" placeholder="Repetir clave" ng-model="usuario.repetirClave" required>
                                <p ng-show="pedidoForm.repetirClave.$invalid && !pedidoForm.repetirClave.$pristine" class="help-block">Repetir clave es requerido.</p>
                            </div>
                            <div class="form-group" ng-class="{ 'has-error' : pedidoForm.telefono.$invalid && !pedidoForm.telefono.$pristine }">
                                <input type="text" class="form-control" name="telefono" id="telefono" placeholder="Tel&eacute;fono" ng-model="usuario.telefono" required>
                                <p ng-show="pedidoForm.telefono.$invalid && !pedidoForm.telefono.$pristine" class="help-block">El tel&eacute;fono es requerido.</p>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Direcci&oacute;n" ng-model="usuario.direccion"></textarea>
                            </div>
                        </form>
                    </tab>
                    <tab select="cambiarUsuarioExiste(true)">
                        <tab-heading>
                            <i class='fa fa-user'></i> Ya estoy registrado
                        </tab-heading>
                        <br>
                        <form role="form" name="usuarioPedidoForm">
                            <div class="form-group" ng-class="{ 'has-error' : usuarioPedidoForm.mail.$invalid && !usuarioPedidoForm.mail.$pristine }">
                                <input type="email" class="form-control" id="email" name="mail" placeholder="Email" ng-model="usuario.mail" required>
                                <p ng-show="usuarioPedidoForm.mail.$invalid && !usuarioPedidoForm.mail.$pristine" class="help-block">El email es requerido.</p>
                            </div>
                            <div class="form-group" ng-class="{ 'has-error' : usuarioPedidoForm.clave.$invalid && !usuarioPedidoForm.clave.$pristine }">
                                <input type="password" class="form-control" id="clave" name="clave" placeholder="Clave" ng-model="usuario.clave" required>
                                <p ng-show="usuarioPedidoForm.clave.$invalid && !usuarioPedidoForm.clave.$pristine" class="help-block">La clave es requerida.</p>
                            </div>
                        </form>
                    </tab>
                </tabset>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-success" ng-class="{hide: usuarioExiste}" ng-disabled="pedidoForm.$invalid" ng-click="submitForm()">
            <i class="fa fa-check"></i> Confirmar</button>
        <button type="submit" class="btn btn-success" ng-class="{hide: !usuarioExiste}" ng-disabled="usuarioPedidoForm.$invalid" ng-click="submitUsuario()">
            <i class="fa fa-check"></i> Confirmar</button>
        <button class="btn btn-default" ng-click="cancel()">Cancel</button>
    </div>

</script>