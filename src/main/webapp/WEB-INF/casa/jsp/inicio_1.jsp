<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>

<div ng-controller="InicioCtrl" ng-init="init('<c:out value="${codigoCasa}"/>')">
    <header class="intro-header" style="background-image: url('casas/{{codigoCasa}}/{{casa.imagen}}')">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                    <div class="site-heading">
                        <h1>{{casa.nombre}}</h1>
                        <hr class="small">
                        <span class="subheading">{{casa.descripcion}}</span>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h3>Nuestras viandas</h3>
            </div>
        </div>

        <div class="row text-center" ng-controller="ViandaCtrl">
            <div class="col-md-9">
                <div class="row">
                    <div class="col-lg-12">
                        <tabset>
                            <tab ng-repeat="categoria in categorias | filter: {empty: false}" heading="{{categoria.nombre}}" 
                                 active="tab.active" disabled="tab.disabled" ng-click="cambiarCategoria(categoria)">
                            </tab>
                        </tabset>
                    </div>
                </div>
                <br>
                <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12 hero-feature" ng-repeat="vianda in viandas | filter : filtroCategoria">
                        <div class="thumbnail cec-pointer">
                            <img ng-if="vianda.imagen" ng-show="vianda.imagen" ng-src="casas/{{codigoCasa}}/img/viandas/{{vianda.imagen}}" alt="{{vianda.nombre}}" ng-click="mostrarVianda(vianda)"/>
                            <img ng-if="vianda.imagen == null" src="temas/heroic/img/sin-imagen.png" alt="{{vianda.nombre}}" ng-click="mostrarVianda(vianda)"/>
                            <div class="caption">
                                <h3 title="{{vianda.nombre}}" ng-click="mostrarVianda(vianda)">{{vianda.nombre}}</h3>                                
                                <form class="form-inline">
                                    <input type="text" class="form-control" size="2" maxlength="2" placeholder="Cant" 
                                           ng-model="vianda.cantidad"/>
                                    <button type="button" class="btn btn-primary" ng-click="agregarAlCarrito(vianda)">
                                        <i class="fa fa-shopping-cart fa-fw"></i>
                                    </button>
                                </form>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-3 thumbnail" ng-controller="CarritoCtrl">
                <p class="lead"><i class="fa fa-shopping-cart fa-fw"></i> Carrito</p>
                <div class="alert alert-info" ng-show="!tieneViandas()">
                    No seleccionó ninguna vianda.
                </div>
                <div ng-show="tieneViandas()">
                    <div class="list-group">
                        <a class="list-group-item" ng-repeat="vianda in carrito.viandas">
                            <span class="pull-left"><strong>{{vianda.cantidad}}</strong></span>
                            <span>{{vianda.nombre}}</span>
                            <button class="close pull-right" ng-click="eliminiarDelCarrito(vianda)">&times;</button></a>
                    </div>
                    <form class="form-inline">
                        <button type="button" class="btn btn-primary" ng-click="abrirFormularioPedido()">
                            <i class="fa fa-check"></i> Pedir
                        </button>   
                        <button type="button" class="btn btn-default" ng-click="vaciarCarrito()">
                            <i class="fa fa-trash-o"></i> Limpiar
                        </button>                                                                                   
                    </form>
                </div>
            </div>

        </div>
    </div>

</div>


    <script type="text/ng-template" id="partials/viandaModal.html">
    <div class="modal-header">
        <button type="button" class="close" aria-hidden="true" ng-click="cerrar()"> &times;</button>
        <h3 class="modal-title">{{vianda.nombre}}</h3>
    </div>    
    <div class="modal-body">
        <div class="row">
            <div class="col-sm-12 col-md-6">
                <div class="thumbnail">
                    <img ng-if="vianda.imagen" ng-show="vianda.imagen" ng-src="casas/{{codigoCasa}}/img/viandas/{{vianda.imagen}}" alt="{{vianda.nombre}}" />
                    <img ng-if="vianda.imagen == null" src="temas/heroic/img/sin-imagen.png" alt="{{vianda.nombre}}" />
                </div>
            </div>
            <div class="col-sm-12 col-md-6">
                <div ng-bind-html="vianda.descripcion"></div>
            </div>
        </div>
    </div>
    </script>    
    
<script type="text/ng-template" id="partials/pedidoModal.html">
    <div class="modal-header">
        <h3 class="modal-title">Complete sus datos</h3>
    </div>
    <div class="modal-body">
        <div class="alert" ng-class="mensaje.tipo === 'error' ? ' alert-danger' : ' alert-success'" ng-show="tieneMensaje()">
            {{mensaje.descripcion}}
        </div>
        <div class="row">
            <div class="col-sm-12">
                <tabset justified="true" type="pills">
                    <tab select="cambiarUsuarioExiste(false)">
                        <tab-heading>
                            <i class='fa fa-asterisk'></i> Primer pedido
                        </tab-heading>
                        <br>
                        <form role="form" name="pedidoForm">
                            <div class="form-group" ng-class="{ 'has-error' : pedidoForm.nombre.$invalid && !pedidoForm.nombre.$pristine }">
                                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre" ng-model="usuario.nombre" required>
                                <p ng-show="pedidoForm.nombre.$invalid && !pedidoForm.nombre.$pristine" class="help-block">El nombre es requerido.</p>
                            </div>
                            <div class="form-group" ng-class="{ 'has-error' : pedidoForm.mail.$invalid && !pedidoForm.mail.$pristine }">
                                <input type="email" class="form-control" id="email" name="mail" placeholder="Email" ng-model="usuario.mail" required>
                                <p ng-show="pedidoForm.mail.$invalid && !pedidoForm.mail.$pristine" class="help-block">El email es requerido.</p>
                            </div>
                            <div class="form-group" ng-class="{ 'has-error' : pedidoForm.clave.$invalid && !pedidoForm.clave.$pristine }">
                                <input type="password" class="form-control" id="clave" name="clave" placeholder="Clave" ng-model="usuario.clave" required>
                                <p ng-show="pedidoForm.clave.$invalid && !pedidoForm.clave.$pristine" class="help-block">La clave es requerida.</p>
                            </div>
                            <div class="form-group" ng-class="{ 'has-error' : pedidoForm.repetirClave.$invalid && !pedidoForm.repetirClave.$pristine }">
                                <input type="password" class="form-control" id="repetirClave" name="repetirClave" placeholder="Repetir clave" ng-model="usuario.repetirClave" required>
                                <p ng-show="pedidoForm.repetirClave.$invalid && !pedidoForm.repetirClave.$pristine" class="help-block">Repetir clave es requerido.</p>
                            </div>
                            <div class="form-group" ng-class="{ 'has-error' : pedidoForm.telefono.$invalid && !pedidoForm.telefono.$pristine }">
                                <input type="text" class="form-control" name="telefono" id="telefono" placeholder="Tel&eacute;fono" ng-model="usuario.telefono" required>
                                <p ng-show="pedidoForm.telefono.$invalid && !pedidoForm.telefono.$pristine" class="help-block">El tel&eacute;fono es requerido.</p>
                            </div>
                            <div class="form-group">
                                <textarea class="form-control" placeholder="Direcci&oacute;n" ng-model="usuario.direccion"></textarea>
                            </div>
                        </form>
                    </tab>
                    <tab select="cambiarUsuarioExiste(true)">
                        <tab-heading>
                            <i class='fa fa-user'></i> Ya estoy registrado
                        </tab-heading>
                        <br>
                        <form role="form" name="usuarioPedidoForm">
                            <div class="form-group" ng-class="{ 'has-error' : usuarioPedidoForm.mail.$invalid && !usuarioPedidoForm.mail.$pristine }">
                                <input type="email" class="form-control" id="email" name="mail" placeholder="Email" ng-model="usuario.mail" required>
                                <p ng-show="usuarioPedidoForm.mail.$invalid && !usuarioPedidoForm.mail.$pristine" class="help-block">El email es requerido.</p>
                            </div>
                            <div class="form-group" ng-class="{ 'has-error' : usuarioPedidoForm.clave.$invalid && !usuarioPedidoForm.clave.$pristine }">
                                <input type="password" class="form-control" id="clave" name="clave" placeholder="Clave" ng-model="usuario.clave" required>
                                <p ng-show="usuarioPedidoForm.clave.$invalid && !usuarioPedidoForm.clave.$pristine" class="help-block">La clave es requerida.</p>
                            </div>
                        </form>
                    </tab>
                </tabset>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="submit" class="btn btn-success" ng-class="{hide: usuarioExiste}" ng-disabled="pedidoForm.$invalid" ng-click="submitForm()">
            <i class="fa fa-check"></i> Confirmar</button>
        <button type="submit" class="btn btn-success" ng-class="{hide: !usuarioExiste}" ng-disabled="usuarioPedidoForm.$invalid" ng-click="submitUsuario()">
            <i class="fa fa-check"></i> Confirmar</button>
        <button class="btn btn-default" ng-click="cancel()">Cancel</button>
    </div>

</script>