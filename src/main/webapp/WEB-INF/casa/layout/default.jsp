<%@page pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<!--[if IE 7 ]><html ng-app="casaApp" class="ie7" lang="en"><![endif]-->
<!--[if IE 8 ]><html ng-app="casaApp" class="ie8" lang="en"><![endif]-->
<!--[if IE 9 ]><html ng-app="casaApp" class="ie9" lang="en"><![endif]-->
<!--[if (gte IE 10)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" ng-app="casaApp" lang="en-US"><!--<![endif]-->
    <head>

        <base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />
        <title>Como Bien! - Gestioná tus pedidos online</title>
        <meta name="format-detection" content="telephone=no" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <!-- Seo Meta -->
        <meta name="description" content="" />
        <meta name="keywords" content="" />

        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="styles/font-awesome.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="styles/prettyPhoto.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="styles/bootstrap.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="styles/owl.carousel.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="styles/owl.theme.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="styles/animate.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="styles/style.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="styles/comobien.css" media="screen" />

        <!-- Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,900,800,700,600,300,200,100,500' rel='stylesheet' type='text/css'/>
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800' rel='stylesheet' type='text/css'/>

        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico"/>

    </head>

    <body>
        <tiles:insertAttribute name="body" />

        <footer id="footer">
            <div class="footer-container">
                <ul class="socials">
                    <li class="facebook"><a href="#" class="circle-icon"><i class="fa fa-facebook"></i></a></li>
                    <li class="twitter"><a href="#" class="circle-icon"><i class="fa fa-twitter"></i></a></li>
                    <li class="flickr"><a href="#" class="circle-icon"><i class="fa fa-flickr"></i></a></li>
                    <li class="youtube"><a href="#" class="circle-icon"><i class="fa fa-youtube"></i></a></li>
                    <li class="rss"><a href="#" class="circle-icon"><i class="fa fa-rss"></i></a></li> 
                    <li class="share-alt"><a href="#" class="circle-icon"><i class="fa fa-share-alt"></i></a></li>         
                    <li class="pinterest"><a href="#" class="circle-icon"><i class="fa fa-pinterest"></i></a></li>
                    <li class="github"><a href="#" class="circle-icon"><i class="fa fa-github"></i></a></li>
                    <li class="google-plus"><a href="#" class="circle-icon"><i class="fa fa-google-plus"></i></a></li>
                    <li class="linkedin"><a href="#" class="circle-icon"><i class="fa fa-linkedin"></i></a></li>
                    <li class="skype"><a href="#" class="circle-icon"><i class="fa fa-skype"></i></a></li>
                    <li class="tumblr"><a href="#" class="circle-icon"><i class="fa fa-tumblr"></i></a></li>
                </ul>	
                <h5 class="footer-copyright">© 2014 Como Bien! - Desarrollado por <a href="http://somospnt.com">somospnt.com</a>.</h5>
            </div>
        </footer>

        <!-- Scripts -->
        <!--[if lt IE 9]>
            <script type="text/javascript" src="scripts/jquery-1.11.0.min.js?ver=1"></script>
        <![endif]-->
        <!--[if (gte IE 9) | (!IE)]><!-->  
        <script type="text/javascript" src="scripts/jquery-2.1.0.min.js?ver=1"></script>
        <!--<![endif]--> 
        <script src="scripts/jquery.easing.js"></script>
        <script type="text/javascript" src="scripts/jquery.prettyPhoto.js"></script>
        <script type="text/javascript" src="scripts/jquery.tools.min.js"></script>
        <script type="text/javascript" src="scripts/owl.carousel.min.js"></script>
        <script type="text/javascript" src="scripts/jquery.nav.js"></script>
        <script type="text/javascript" src="scripts/jquery.scrollTo.js"></script>
        <script type="text/javascript" src="scripts/jquery.sticky.js"></script>
        <script type="text/javascript" src="scripts/jquery.appear.js"></script>
        <script type="text/javascript" src="scripts/responsiveslides.min.js"></script>
        <script type="text/javascript" src="scripts/verge.min.js"></script>
        <script type="text/javascript" src="scripts/custom.js"></script>

        <script src="temas/<c:out value="${tema}"/>/js/lib/angular.min.js"></script>
        <script src="temas/<c:out value="${tema}"/>/js/lib/angular-route.min.js"></script>
        <script src="temas/<c:out value="${tema}"/>/js/lib/angular-sanitize.min.js"></script>
        <script src="temas/<c:out value="${tema}"/>/js/lib/ui-bootstrap-tpls-0.11.0.min.js"></script>        
        <script src="temas/<c:out value="${tema}"/>/js/app.js"></script>
        <script src="temas/<c:out value="${tema}"/>/js/config.js"></script>
        <script src="temas/<c:out value="${tema}"/>/js/services.js"></script>
        <script src="temas/<c:out value="${tema}"/>/js/controllers.js"></script>
        <script src="temas/<c:out value="${tema}"/>/js/filters.js"></script>
        <script src="temas/<c:out value="${tema}"/>/js/directives.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-55375170-1', 'auto');
            ga('send', 'pageview');

        </script>

    </body>
</html>
