<%@page contentType="text/html" pageEncoding="UTF-8"%>
<toaster-container></toaster-container>


<section class="wrapper" ng-show="mostrarError">                
    <%@ include file="/WEB-INF/admin/jsp/error.jsp" %> 
</section>    
<section class="wrapper" ng-show="!mostrarError">
    <h3><i class="fa fa-angle-right"></i> Men&uacute;s - {{casa.codigoCasa}}</h3>
    <p>
        <button type="button" class="btn btn-primary btn-lg" ng-click="nuevoMenuModal()">Nuevo menú</button>
    </p>
    <div class="row mt">
        <div class="col-md-12">
            <div class="content-panel showback">
                <div class="row">
                    <div class="col-md-2">
                        <h4>
                            <i class="fa fa-angle-right"></i>Men&uacute;s disponibles                    
                        </h4>                 
                    </div>
                    <div class="col-md-10">
                        <form class="navbar-form cec-buscador" role="search">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                <input type="text" class="form-control" placeholder="Buscar..." ng-model="filtro">                                
                            </div>
                        </form>
                    </div>
                </div>
                <hr/>
                <div class="alert alert-info" ng-show="!menus.length">No se encontraron <b>Men&uacute;s</b> registrados</div>
                <table class="table table-striped table-advance table-hover" ng-show="menus.length">
                    <thead>
                        <tr>
                            <th><i class="fa fa-cutlery"></i> Nombre</th>
                            <th class="hidden-phone"><i class="fa fa-list"></i> Categor&iacute;a</th>
                            <th class="hidden-phone"><i class="fa fa-eye"></i> Visible</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="menu in menus | filter:filtro">
                            <td>{{menu.nombre}}</td>
                            <td class="hidden-phone">
                                <span ng-if="menu.categoria">{{menu.categoria.nombre}}</span>
                                <span ng-if="!menu.categoria">Sin categor&iacute;a</span>
                            </td>
                            <td><input type="checkbox" ng-model="menu.visible" disabled="disabled"/></td>
                            <td>
                                <!--<button class="btn btn-success btn-xs"><i class="fa fa-check"></i></button>-->
                                <button class="btn btn-primary btn-xs" ng-click="editarMenuModal(menu)"><i class="fa fa-pencil"></i></button>
                                <button class="btn btn-danger btn-xs" ng-click="borrarMenuModal(menu)"><i class="fa fa-trash-o "></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div><!-- /content-panel -->
        </div><!-- /col-md-12 -->
    </div>
</section>

<script type="text/ng-template" id="menu/form.html">
    <div class="modal-header">
        <button type="button" class="close" aria-hidden="true" ng-click="cerrar()"> &times;</button>
        <h4 class="modal-title" id="myModalLabel">{{titulo}}</h4>
    </div>
    <form name="formulario" class="form-horizontal style-form" method="post" enctype="multipart/form-data" novalidate>
        <div class="modal-body">
            <div class="form-group" ng-class="{'has-error': dirtyAndInvalid(formulario.nombre)}">
                <div class="col-sm-8">
                    <input name="nombre" type="text" class="form-control" placeholder="Nombre" ng-model="menu.nombre" required>
                </div>
                <div class="col-sm-3">
                    <select ng-model="menu.categoria" ng-options="categoria.nombre for categoria in categorias track by categoria.id" class="form-control">
                        <option value="">-- Sin categoria --</option>
                    </select>
                </div>
                <div class="col-sm-1">
                    <label class="checkbox">
                        <input type="checkbox" ng-model="menu.visible">
                        Visible
                    </label>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-8 col-xs-12">
                    <div text-angular required ng-model="menu.descripcion" 
                        ta-toolbar-class="btn-toolbar" 
                        ta-toolbar-group-class="btn-group" 
                        ta-toolbar-button-class="btn btn-default" 
                        ta-toolbar-button-active-class="active"
                        ta-toolbar="[['h1','h2','h3'],['bold','italics', 'underline'], ['ul', 'ol'], ['justifyLeft', 'justifyCenter', 'justifyRight'], ['undo', 'redo']]">
                    </div>
                </div>
                <div class="col-md-4 col-sm-4 col-xs-12 hero-feature">
                    <div flow-init="{singleFile:true}" flow-file-added="!!{png:1,gif:1,jpg:1,jpeg:1}[$file.getExtension()]" flow-name="form.archivo">
                        <div class="thumbnail" ng-show="!existeImagen()">
                          <!--<img src="http://www.placehold.it/250x190/EFEFEF/AAAAAA&amp;text=Sin+imagen">-->
                          <img src="admin/static/img/sin-imagen.gif">
                        </div>                                                        
                        <div class="thumbnail" ng-show="existeImagen()">
                          <img app-flow-img="$flow.files[0]" app-flow-img-change="actualizarImagenACargar" ng-src="{{imagen.data}}" />
                        </div>
                        <div>
                          <span class="btn btn-primary" ng-show="!$flow.files.length" flow-btn>Subir imagen</span>  
                          <span class="btn btn-danger ng-hide" ng-show="existeImagen()" ng-click="borrarImagen($flow)">Eliminar</span>
                        </div>                            
                    </div>                        
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary" data-loading-text="Guardando..." btn-loading="guardando" ng-disabled="formulario.$invalid" ng-click="guardar()">Guardar</button>
            <button type="button" class="btn btn-default" ng-click="cerrar()">Cerrar</button>
        </div>
    </form>
</script>

<script type="text/ng-template" id="menu/confirm.html">
    <div class="modal-header">
    <button type="button" class="close" aria-hidden="true" ng-click="cerrar()"> &times;</button>
    <h4 class="modal-title" id="myModalLabel">{{titulo}}</h4>
    </div>
    <div class="modal-body">
    <p>¿Estás seguro que querés eliminar el menú "{{menu.nombre}}"?</p>
    </div>
    <div class="modal-footer">
    <button type="submit" class="btn btn-danger" data-loading-text="Eliminando..." btn-loading="eliminando" ng-click="eliminar()">Eliminar</button>
    <button type="button" class="btn btn-default" ng-click="cerrar()">Cerrar</button>
    </div>
</script>
