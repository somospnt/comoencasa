<%@page contentType="text/html" pageEncoding="UTF-8"%>
<toaster-container></toaster-container>

<section class="wrapper" ng-show="mostrarError">                
        <%@ include file="/WEB-INF/admin/jsp/error.jsp" %> 
</section>
<section class="wrapper" ng-show="!mostrarError">
    
    <h3 ng-switch="estado">
        <span ng-switch-when="pendiente"><i class="fa fa-angle-right"></i> Pedidos pendientes</span>
        <span ng-switch-default><i class="fa fa-angle-right"></i> Todos los pedidos</span>
    </h3>
    
    <p ng-show="{{estado === 'pendiente'}}">        
        <button type="button" class="btn btn-primary btn-lg" ng-click="generarComanda()" data-loading-text="Generando Comanda..." btn-loading="generandoComanda">
            Generar Comanda <span class="fa fa-external-link-square"></span> 
        </button>
    </p>
    <div class="row mt">
        <div class="col-md-12">
            <div class="content-panel showback">
                <div class="row">
                    <div class="col-md-2">
                        <h4>
                            <i class="fa fa-angle-right"></i>Listado de pedidos
                        </h4>                           
                    </div>
                    <div class="col-md-10">
                        <form class="navbar-form cec-buscador" role="search">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                <input type="text" class="form-control" placeholder="Buscar..." ng-model="filtro">                                
                            </div>
                        </form>
                    </div>
                </div>
                <hr>
                <div class="alert alert-info" ng-show="!pedidos.length">No se encontraron <b>Pedidos</b> registrados</div>
                <table class="table table-striped table-advance table-hover" ng-show="pedidos.length">
                    <thead>
                        <tr>
                            <th><i class="fa fa-calendar"></i> Fecha</th>
                            <th><i class="fa fa-user"></i> Para</th>
                            <th class="hidden-phone"><i class="fa fa fa-envelope-o"></i> E-Mail</th>
                            <th class="hidden-phone"><i class="fa fa-phone"></i> Tel&eacute;fono</th>                                                        
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="pedido in pedidos | filter:filtro" ng-click="verDetallePedido(pedido)" class="click-over">
                            <td>{{pedido.fecha | date : 'dd/MM/yyyy'}}</td>
                            <td>{{pedido.usuario.nombre}}</td>
                            <td class="hidden-phone">{{pedido.usuario.mail}}</td>
                            <td class="hidden-phone">{{pedido.usuario.telefono}}</td>
                        </tr>
                    </tbody>
                </table>
            </div><!-- /content-panel -->
        </div><!-- /col-md-12 -->
    </div>
</section>

<script type="text/ng-template" id="pedido/detalle.html">
    <div class="modal-header">
        <button type="button" class="close" aria-hidden="true" ng-click="cerrar()"> &times;</button>
        <h4 class="modal-title" id="myModalLabel">Pedido para {{pedido.usuario.nombre}}</h4>
    </div>
    <form class="form-horizontal style-form" method="post" enctype="multipart/form-data" ng-submit="guardar()">
        <div class="modal-body">        
            <table class="table table-striped table-advance table-hover">
                <thead>
                    <tr>
                        <th><i class="fa fa-calendar"></i></th>                        
                        <th><i class="fa fa-user"></i></th>
                        <th><i class="fa fa fa-envelope-o"></i></th>
                        <th><i class="fa fa-phone"></i></th>                                                        
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{pedido.fecha | date : 'dd/MM/yyyy'}}</td>
                        <td>{{pedido.usuario.nombre}}</td>
                        <td>{{pedido.usuario.mail}}</td>
                        <td>{{pedido.usuario.telefono}}</td>
                    </tr>
                </tbody>
            </table>
            <br/>
            <h4><i class="fa fa-angle-right"></i> Detalle: </h4>
            <hr/>
            <table class="table table-striped table-advance table-hover">
                <thead>
                    <tr>
                        <th>Cantidad</th>
                        <th>Descripci&oacute;n</th>
                    </tr>
                </thead>                
                <tbody>
                    <tr ng-repeat="pedidoItem in pedido.items">
                        <td>{{pedidoItem.cantidad}}</td>
                        <td>{{pedidoItem.menu.nombre}}</td>
                    </tr>
                </tbody>
            </table>    
            <p><strong>Total de Items: </strong> {{calcularCantidadItem(pedido)}}</p>
        </div>
        <div class="modal-footer">            
            <button type="button" class="btn btn-default" ng-click="cerrar()">Cerrar</button>
        </div>
    </form>
</script>
