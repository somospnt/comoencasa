<%@page contentType="text/html" pageEncoding="UTF-8"%>
<toaster-container></toaster-container>

<section class="wrapper" ng-show="mostrarError">                
        <%@ include file="/WEB-INF/admin/jsp/error.jsp" %> 
</section>
<section class="wrapper" ng-show="!mostrarError">
    <h3><i class="fa fa-angle-right"></i> Clientes</h3>
    <div class="row mt">
        <div class="col-md-12">
            <div class="content-panel showback">
                <div class="row">
                    <div class="col-md-2">
                        <h4>
                            <i class="fa fa-angle-right"></i>Listado de clientes
                        </h4>                 
                    </div>
                    <div class="col-md-10">
                        <form class="navbar-form cec-buscador" role="search">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-search"></i></span>
                                <input type="text" class="form-control" placeholder="Buscar..." ng-model="filtro">                                
                            </div>
                        </form>
                    </div>
                </div>
                <hr>                
                <div class="alert alert-info" ng-show="!clientes.length">No se encontraron <b>Clientes</b> registrados</div>
                <table class="table table-striped table-advance table-hover" ng-show="clientes.length">
                    <thead>
                        <tr>                            
                            <th><i class="fa fa-user"></i> Nombre</th>
                            <th class="hidden-phone"><i class="fa fa fa-envelope-o"></i> E-Mail</th>
                            <th class="hidden-phone"><i class="fa fa-phone"></i> Tel&eacute;fono</th>
                            <th class="hidden-phone"><i class="fa fa-send"></i> Direcci&oacute;n</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr ng-repeat="cliente in clientes | filter:filtro" class="click-over" ng-click="mostrarFicha(cliente)">
                            <td>{{cliente.usuario.nombre}}</td>
                            <td class="hidden-phone">{{cliente.usuario.mail}}</td>
                            <td class="hidden-phone">{{cliente.usuario.telefono}}</td>
                            <td class="hidden-phone">{{cliente.usuario.direccion}}</td>
                        </tr>
                    </tbody>
                </table>
            </div><!-- /content-panel -->
        </div><!-- /col-md-12 -->
    </div>
</section>


<script type="text/ng-template" id="cliente/ficha.html">
    <div class="modal-header">
        <button type="button" class="close" aria-hidden="true" ng-click="cerrar()"> &times;</button>
        <h4 class="modal-title" id="myModalLabel"> <i class="fa fa-folder"></i> Ficha de {{cliente.usuario.nombre}}</h4>
    </div>
    <div class="modal-body">        
        <div class="showback">
            <h4><i class="fa fa-angle-right"></i> Datos</h4>
            <ul>
                <li><i class="fa fa-user"></i> {{cliente.usuario.nombre}}</li>
                <li><i class="fa fa fa-calendar"></i> {{cliente.fechaAlta | date : 'dd/MM/yyyy'}}</li>
                <li><i class="fa fa fa-envelope-o"></i> {{cliente.usuario.mail}}</li>
                <li><i class="fa fa fa fa-phone"></i> {{cliente.usuario.telefono}}</li>
                <li><i class="fa fa fa-send"></i> {{cliente.usuario.direccion}}</li>
            </ul>
        </div>            
        <div class="showback">
            <h4><i class="fa fa-angle-right"></i> Notas</h4>
            <div text-angular ng-model="form.notas" 
                ta-toolbar-class="btn-toolbar" 
                ta-toolbar-group-class="btn-group" 
                ta-toolbar-button-class="btn btn-default" 
                ta-toolbar-button-active-class="active"
                ta-toolbar="[['h1','h2','h3'],['bold','italics', 'underline'], ['ul', 'ol'], ['justifyLeft', 'justifyCenter', 'justifyRight'], ['undo', 'redo']]"
            ></div>            
        </div>
    </div>
    <div class="modal-footer">   
        <button type="submit" class="btn btn-primary" data-loading-text="Guardando..." btn-loading="guardandoCliente" ng-click="guardar()">Guardar</button>
        <button type="button" class="btn btn-default" ng-click="cerrar()">Cerrar</button>
    </div>
    
</script>