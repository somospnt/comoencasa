<%@page pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html lang="es" ng-app="comoEnCasaAdmin">
    <head>
        <base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Como en casa</title>
        <link rel="icon" type="image/x-icon" href="img/favicon.ico" />        

        <link href="admin/static/css/bootstrap.css" rel="stylesheet" />

        <!--external css-->
        <link href="admin/static/font-awesome/css/font-awesome.css" rel="stylesheet" />       
        <link href="admin/static/bower_components/AngularJS-Toaster/toaster.css" rel="stylesheet" type="text/css" />
        <link href="admin/static/lineicons/style.css" rel="stylesheet" type="text/css"/>    

        <!-- Custom styles for this template -->
        <link href="admin/static/css/style.css" rel="stylesheet"/>
        <link href="admin/static/css/style-responsive.css" rel="stylesheet"/>
        <link href="admin/static/css/ng-dashgumfree.css" rel="stylesheet"/>
    </head>
    <body ng-controller="AppCtrl" ng-init="init('<c:out value="${casa.codigo}"/>', '<c:out value="${casa.nombre}"/>')">
        <section id="container" >
            <header class="header black-bg" >
                <div class="sidebar-toggle-box" ng-click="isCollapsed = !isCollapsed">
                    <div class="fa fa-bars tooltips" data-placement="right" tooltip="Toggle Navigation"></div>
                </div>
                <!--logo start-->
                <a href="index.html" class="logo"><b>Como en casa</b></a>            
                <!--logo end-->
                <div class="top-menu">
                    <ul class="nav pull-right top-menu">
                        <li><a class="logout" href="logout">Salir</a></li>
                    </ul>
                </div>
            </header>

            <!-- **********************************************************************************************************************************************************
          MAIN SIDEBAR MENU
          *********************************************************************************************************************************************************** -->
            <!--sidebar start-->
            <aside ng-controller="MenuCtrl" ng-init="init('menus')">
                <div id="sidebar" gm-siderbar-collapse="isCollapsed" gm-siderbar-collapse-content="#main-content">
                    <!-- sidebar menu start-->
                    <ul class="sidebar-menu" id="nav-accordion">

                        <p class="centered"><a href="#dashboard"><img src="admin/static/img/comoencasa-logo.png" class="img-circle" width="60"></a></p>
                        <h5 class="centered">Panel de control</h5>

                        <li class="mt">
                            <a href="admin/inicio.htm#/menus" ng-class="{active: isSelected('menus')}"  title="Menus" ng-click="seleccionarMenu('menus')">
                                <i class="fa fa-cutlery"></i>
                                <span>Menus</span>
                            </a>                            
                        </li>
                        <li class="sub-menu">
                            <a ng-click="seleccionarMenu('pedidos')" ng-class="{active: isSelected('pedidos')}">
                                <i class="fa fa-file"></i>
                                <span>Pedidos</span>
                            </a>
                            <ul collapse="!isExpanded('pedidos')" ng-class="{sub: isExpanded('pedidos')}">
                                <li ng-class="{active: isSubItemSelected('pedidos-pendientes')}"><a  href="admin/inicio.htm#/pedidos/pendiente" ng-click="seleccionarSubItem('pedidos-pendientes')">Pendientes</a></li>
                                <li ng-class="{active: isSubItemSelected('pedidos-todos')}"><a  href="admin/inicio.htm#/pedidos" ng-click="seleccionarSubItem('pedidos-todos')">Todos</a></li>                          
                            </ul>
                        </li>
                        <li class="sub-menu">
                            <a href="admin/inicio.htm#/clientes" ng-class="{active: isSelected('clientes')}"  title="Clientes" ng-click="seleccionarMenu('clientes')">
                                <i class="fa fa-users"></i>
                                <span>Clientes</span>
                            </a>                            
                        </li>
                    </ul>
                    <!-- sidebar menu end-->
                </div>
            </aside>
            <!--sidebar end-->
            <!--<section id="main-content">
                
            </section>-->
            <span us-spinner="{radius:30, width:8, length: 16}" spinner-key="spinner-1"></span>
            <tiles:insertAttribute name="body" />
            <!--footer start-->
            <footer class="site-footer">
                <div class="text-center">
                    Copyright © Como En Casa 2014
                    <!--                    <a href="#dashboard" class="go-top">
                                            <i class="fa fa-angle-up"></i>
                                        </a>-->
                </div>
            </footer>
            <!--footer end-->
        </section>  




        <!-- Jquery plugins -->
        <script src="admin/static/bower_components/jquery/dist/jquery.min.js"></script>       
        <script src="admin/static/bower_components/sparklines/source/sparkline.js"></script>
        <script src="admin/static/js/ext/bootstrap.min.js"></script>


        <!-- angularjs & jqueryWrapper -->
        <script src="admin/static/bower_components/angular/angular.min.js"></script>
        <script src="admin/static/bower_components/angular-route/angular-route.min.js"></script>  
        <script src="admin/static/bower_components/angular-animate/angular-animate.min.js"></script>
        <script src="admin/static/bower_components/angular-bootstrap/ui-bootstrap.min.js"></script>  
        <script src="admin/static/bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>          
        <script src="admin/static/bower_components/ng-flow/dist/ng-flow-standalone.min.js"></script>  
        <script src="admin/static/bower_components/AngularJS-Toaster/toaster.js"></script>  
        <script src="admin/static/bower_components/spin.js/spin.js"></script>
        <script src="admin/static/bower_components/angular-spinner/angular-spinner.js"></script>
        <script src="admin/static/bower_components/textAngular/dist/textAngular-sanitize.min.js"></script>
        <script src="admin/static/bower_components/textAngular/dist/textAngular.min.js"></script>

        <script src="admin/static/js/ui-bootstrap-ext.js"></script>
        <script src="admin/static/js/lib-wrapper.js"></script>
        <script src="admin/static/js/app.js"></script>
        <script src="admin/static/js/config.js"></script>
        <script src="admin/static/js/controllers.js"></script>
        <script src="admin/static/js/services.js"></script>
        <script src="admin/static/js/directives.js"></script>        
        <script>
                                        (function (i, s, o, g, r, a, m) {
                                            i['GoogleAnalyticsObject'] = r;
                                            i[r] = i[r] || function () {
                                                (i[r].q = i[r].q || []).push(arguments)
                                            }, i[r].l = 1 * new Date();
                                            a = s.createElement(o),
                                                    m = s.getElementsByTagName(o)[0];
                                            a.async = 1;
                                            a.src = g;
                                            m.parentNode.insertBefore(a, m)
                                        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

                                        ga('create', 'UA-55375170-1', 'auto');
                                        ga('send', 'pageview');

        </script>
    </body>
</html>
