<%@page pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html lang="es">
    <head>      
        <base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Como en casa</title>
        <link rel="icon" type="image/x-icon" href="img/favicon.ico" />        
        
    </head>
    <body>
        <h1>Login</h1>
        <div class="center-block">
                <div class="login-block">
                    <form id="login-form" class="orb-form" method="POST" action="login">
                        <header>
                            <div class="image-block"><img src="images/logo.png" alt="" width="200"/></div>
                            Bienvenido a Como en Casa <small>Si no tenés cuenta, <a href="mailto:contacto@somospnt.com">avisanos</a></small>
                        </header>
                        <c:if test="${param.error != null}">
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"><i class="fa fa-times-circle"></i></button>
                                <strong>¡Upa!</strong> El nombre de usuario o la contraseña son incorrectas.
                            </div>
                        </c:if>
                        <fieldset>
                            <section>
                                <div class="row">
                                    <label class="label col col-4">Usuario</label>
                                    <div class="col col-8">
                                        <label class="input"> <i class="icon-append fa fa-user"></i>
                                            <input type="text" id="usuario" name="username" placeholder="Usuario">
                                        </label>
                                    </div>
                                </div>
                            </section>
                            <section>
                                <div class="row">
                                    <label class="label col col-4">Contraseña</label>
                                    <div class="col col-8">
                                        <label class="input"> <i class="icon-append fa fa-lock"></i>
                                            <input type="password" id="password" name="password" placeholder="Contraseña">
                                        </label>
                                    </div>
                                </div>
                            </section>
                            <section>
                                <div class="row">
                                    <div class="col col-4"></div>
                                    <div class="col col-8">
                                        <label class="checkbox">
                                            <input type="checkbox" name="remember-me" checked>
                                            <i></i>Recordarme</label>
                                    </div>
                                </div>
                            </section>
                        </fieldset>
                        <footer>
                            <input type="submit" class="btn btn-default" value="Entrar"/>
                        </footer>
                    </form>
                </div>
                <div class="copyrights"> IDEADO Y DESARROLLADO POR <a target="_BLANK" href="http://www.somospnt.com">PNT</a> <br>
                    El equipo de desarrollo de <a target="_BLANK" href="http://www.connectis-ict.com.ar">Connectis Argentina</a> </div>
                </p>
            </div>
    </body>
</html>
