<%@page pageEncoding="UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>

<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie7" lang="en"><![endif]-->
<!--[if IE 8 ]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9 ]><html class="ie9" lang="en"><![endif]-->
<!--[if (gte IE 10)|!(IE)]><!--><html xmlns="http://www.w3.org/1999/xhtml" lang="en-US"><!--<![endif]-->
    <head>
        <base href="${fn:substring(url, 0, fn:length(url) - fn:length(pageContext.request.requestURI))}${pageContext.request.contextPath}/" />
        <title>Como Bien! - Gestioná tus pedidos online</title>
        <meta name="format-detection" content="telephone=no" />
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <!-- Seo Meta -->
        <meta name="description" content="" />
        <meta name="keywords" content="" />

        <!-- Styles -->
        <link rel="stylesheet" type="text/css" href="styles/font-awesome.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="styles/prettyPhoto.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="styles/bootstrap.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="styles/owl.carousel.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="styles/owl.theme.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="styles/animate.min.css" media="screen" />
        <link rel="stylesheet" type="text/css" href="styles/style.css" media="screen" />

        <!-- Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,900,800,700,600,300,200,100,500' rel='stylesheet' type='text/css' />
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,800' rel='stylesheet' type='text/css' />

        <!-- Favicon -->
        <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />

    </head>

    <body>

        <header id="header">
            <div id="logo" class="text-center animated" data-animation="fadeInUp" data-animation-delay="400">
                <a href="index.html"><img src="images/logo.png" alt="Logo"></a>
            </div><!-- end logo -->
            <div id="slideshow">
                <ul class="rslides">
                    <li style="background-image: url(images/slide_img_4.jpg);">
                        <img src="images/slide_img_4.jpg" alt="">
                            <div class="slideshow-caption">
                                <h1>Gestioná tus <span class="highlight">pedidos online</span></h1>
                                <h3>Ideal para vianderos</h3>
                                <div class="button">
                                    <a href="#contactanos">Quiero probarlo</a>
                                </div>
                            </div><!-- end .slideshow-caption -->
                    </li><!-- end .slideshow item -->
                    <li style="background-image: url(images/slide_img_3.jpg);">
                        <img src="images/slide_img_3.jpg" alt="">
                            <div class="slideshow-caption">
                                <h1>Tus <span class="highlight">menús</span> al día</h1>
                                <h3>Ideal para vianderos</h3>
                                <div class="button">
                                    <a href="#contactanos">Quiero probarlo</a>
                                </div>
                            </div><!-- end .slideshow-caption -->
                    </li><!-- end .slideshow item -->
                    <li style="background-image: url(images/slide_img_5.jpg);">
                        <img src="images/slide_img_5.jpg" alt="">
                            <div class="slideshow-caption">
                                <h1>Armá tu <span class="highlight">tienda online</span></h1>
                                <h3>Ideal para vianderos</h3>
                                <div class="button">
                                    <a href="#contactanos">Quiero probarlo</a>
                                </div>
                            </div><!-- end .slideshow-caption -->
                    </li><!-- end .slideshow item -->	  
                </ul>	
            </div><!-- end slideshow -->
        </header>

        <nav id="navigation">
            <div class="nav-container">	
                <ul>
                    <li><a class="active" href="#bienvenido"><span class="extrabold">Bienvenido</span></a></li>
                    <li><a href="#demostracion"><span class="extrabold">Demostración</span></a></li>
                    <li><a href="#nosotros"><span class="extrabold">Nosotros</span></a></li>
                    <!--<li><a href="#special-gallery">Daily <span class="extrabold">Specialties</span></a></li>-->			
                    <!--<li><a href="#big-menu">Our <span class="extrabold">Menu</span></a></li>
                    <li><a href="#cooks">Our <span class="extrabold">Cooks</span></a></li>
                    <li><a href="#blog">Our <span class="extrabold">Blog</span></a></li>-->
                    <li><a href="#contactanos"><span class="extrabold">Contactanos</span></a></li>
                </ul>			
            </div><!-- end .nav-container -->
        </nav>

        <nav id="mobile-navigation">
            <div class="mobile-nav-container">
                <div id="menu-toggle">
                    <i class="fa fa-bars"></i>
                </div>		
                <ul class="inactive">
                    <li><a class="active" href="#bienvenido"><span class="extrabold">Bienvenido</span></a></li>
                    <li><a href="#demostracion"><span class="extrabold">Demostración</span></a></li>
                    <li><a href="#nosotros"><span class="extrabold">Nosotros</span></a></li>
                    <!--<li><a href="#special-gallery">Daily <span class="extrabold">Specialties</span></a></li>-->			
                    <!--<li><a href="#big-menu">Our <span class="extrabold">Menu</span></a></li>
                    <li><a href="#cooks">Our <span class="extrabold">Cooks</span></a></li>
                    <li><a href="#blog">Our <span class="extrabold">Blog</span></a></li>-->
                    <li><a href="#contactanos"><span class="extrabold">Contactanos</span></a></li>
                </ul>			
            </div><!-- end .mobile-nav-container -->
        </nav>

        <tiles:insertAttribute name="body" />

        <footer id="footer">
            <div class="footer-container">
                <ul class="socials">
                    <li class="facebook"><a href="https://www.facebook.com/ConnectisIctArg" class="circle-icon"><i class="fa fa-facebook"></i></a></li>
                    <li class="twitter"><a href="https://twitter.com/ConnectisICTArg" class="circle-icon"><i class="fa fa-twitter"></i></a></li>
                    <!-- <li class="flickr"><a href="#" class="circle-icon"><i class="fa fa-flickr"></i></a></li> -->
                    <!-- <li class="youtube"><a href="#" class="circle-icon"><i class="fa fa-youtube"></i></a></li> -->
                    <!-- <li class="rss"><a href="#" class="circle-icon"><i class="fa fa-rss"></i></a></li>  -->
                    <!-- <li class="share-alt"><a href="#" class="circle-icon"><i class="fa fa-share-alt"></i></a></li>          -->
                    <!--<li class="pinterest"><a href="#" class="circle-icon"><i class="fa fa-pinterest"></i></a></li>-->
                    <!--<li class="github"><a href="#" class="circle-icon"><i class="fa fa-github"></i></a></li>-->
                    <!--<li class="google-plus"><a href="#" class="circle-icon"><i class="fa fa-google-plus"></i></a></li>-->
                    <li class="linkedin"><a href="https://www.linkedin.com/company/3188246?trk=tyah" class="circle-icon"><i class="fa fa-linkedin"></i></a></li>
                    <!--<li class="skype"><a href="#" class="circle-icon"><i class="fa fa-skype"></i></a></li>-->
                    <!--<li class="tumblr"><a href="#" class="circle-icon"><i class="fa fa-tumblr"></i></a></li>-->                    
                </ul>	
                <h5 class="footer-copyright">© 2014 Como Bien! - Desarrollado por <a href="http://somospnt.com">somospnt.com</a>.</h5>
            </div>
        </footer>

        <!-- Scripts -->
        <!--[if lt IE 9]>
            <script type="text/javascript" src="scripts/jquery-1.11.0.min.js?ver=1"></script>
        <![endif]-->
        <!--[if (gte IE 9) | (!IE)]><!-->  
        <script type="text/javascript" src="scripts/jquery-2.1.0.min.js?ver=1"></script>
        <!--<![endif]--> 
        <script src="scripts/jquery.easing.js"></script>
        <script type="text/javascript" src="scripts/jquery.prettyPhoto.js"></script>
        <script type="text/javascript" src="scripts/jquery.tools.min.js"></script>
        <script type="text/javascript" src="scripts/owl.carousel.min.js"></script>
        <script type="text/javascript" src="scripts/jquery.nav.js"></script>
        <script type="text/javascript" src="scripts/jquery.scrollTo.js"></script>
        <script type="text/javascript" src="scripts/jquery.sticky.js"></script>
        <script type="text/javascript" src="scripts/jquery.appear.js"></script>
        <script type="text/javascript" src="scripts/responsiveslides.min.js"></script>
        <script type="text/javascript" src="scripts/verge.min.js"></script>
        <script type="text/javascript" src="scripts/custom.js"></script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-55375170-1', 'auto');
            ga('send', 'pageview');

        </script>

    </body>
</html>