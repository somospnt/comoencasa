<style>
.cartel-error h1 {
    font-size: 8em;
}

.cartel-error p {
    font-size: 2em;
}
</style>
<section>
    <div class="container">
        <div class="row mt cartel-error">
            <div class="col-md-3">
                <img src="img/comoencasa-logo.png" class="img-circle pull-right" width="280"> 
            </div>
            <div class="col-md-9">
                <h1>Oooopps!</h1>
                <p>Ocurri&oacute; un error, por favor intente m&aacute;s tarde</p>
            </div>
        </div>
    </div>
</section>
<footer class="text-center">
            <div class="footer-above">
                <div class="container">
                    <div class="row">
                        <div class="footer-col col-md-4">
                            <h3>Connectis Argentina</h3>
                            <p>Bulnes 2756.<br>Ciudad Aut�noma de Buenos Aires<br><i class="fa fa-phone"></i> + 54 11 5556-5100 </p>
                        </div>
                        <div class="footer-col col-md-4">
                            <h3>En la Web</h3>
                            <ul class="list-inline">
                                <li>
                                    <a href="https://www.facebook.com/ConnectisIctArg" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-facebook"></i></a>
                                </li>
                                <li>
                                    <a href="https://www.linkedin.com/company/3188246?trk=tyah" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-linkedin"></i></a>
                                </li>
                                <li>
                                    <a href="https://twitter.com/ConnectisICTArg" class="btn-social btn-outline" target="_blank"><i class="fa fa-fw fa-twitter"></i></a>
                                </li>
                            </ul>
                        </div>
                        <div class="footer-col col-md-4">
                            <h3>Acerca Como En Casa</h3>
                            <p>En esta primera etapa de desarrollo estamos invitando a vianderos a probar el sitio y ayudarnos a mejorarlo en base a sus necesidades.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer-below">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            Copyright &copy; Como En Casa 2014
                        </div>
                    </div>
                </div>
            </div>
        </footer>