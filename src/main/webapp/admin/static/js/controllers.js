'use strict';

/* Controllers */

angular.module('comoEnCasaAdmin.controllers', [])
.controller('AppCtrl', ['$scope', 'SessionService', 'CategoriaService', function($scope, SessionService, CategoriaService) {
        $scope.init = function(codigoCasa) {
            var casa = {
                codigoCasa: codigoCasa
            };
            SessionService.setValor(SessionService.CASA, casa);

            CategoriaService.buscarCategorias().then(
                function(categorias){
                    SessionService.setValor(SessionService.CATEGORIAS, categorias);
                }, function(error){
                    console.log("Error al obtener las categorias: " + error);
                });
        };
    }])

.controller('MenuCtrl', ['$scope', '$location', function ($scope, $location) {
        var itemExpandState = false;
        var selectedItem;
        var selectedSubItem;


        $scope.init = function (menuInicial) {
            selectedItem = menuInicial || $location.path().substr(1);
        };
        $scope.isSelected = function (key) {
            return (selectedItem === key);
        };
        $scope.isSubItemSelected = function(key) {
            return (selectedSubItem === key);
        };
        $scope.isExpanded = function(key) {
            if (selectedItem === key) {
                return itemExpandState;
            }
            return false;
        };

        $scope.seleccionarMenu = function (key) {
            if (selectedItem !== key) {
                itemExpandState = true;
                selectedSubItem = '';
            } else {
                itemExpandState = !itemExpandState;
            }
            selectedItem = key;
        };
        $scope.seleccionarSubItem = function(key) {
            selectedSubItem = key;
        };

    }])

.controller('MenusCtrl', ['$scope', '$modal', 'toaster', 'SessionService', 'MenuService', 'usSpinnerService', function($scope, $modal, toaster, SessionService, MenuService, usSpinnerService) {
        $scope.casa = SessionService.getValor(SessionService.CASA);
        $scope.mostrarError = false;
        usSpinnerService.spin('spinner-1');
        MenuService.getMenus($scope.casa.codigoCasa).then(
                function (menus) {
                    $scope.menus = menus;
                    usSpinnerService.stop('spinner-1');
                }, function (error) {
            console.log("Error al obtener los menus: " + error);
            usSpinnerService.stop('spinner-1');
            $scope.mostrarError = true;
        });

        $scope.nuevoMenuModal = function () {
            var modalInstance = $modal.open({
                templateUrl: 'menu/form.html',
                controller: 'NuevoMenuModalCtrl',
                size: 'lg',
                resolve: {
                    data: function () {
                        return {
                            titulo: "Nuevo menú",
                            edicion: false,
                            menu: {visible: true}
                        };
                    }
                }
            });
        };

        $scope.editarMenuModal = function (menu) {
            var modalInstance = $modal.open({
                templateUrl: 'menu/form.html',
                controller: 'NuevoMenuModalCtrl',
                size: 'lg',
                resolve: {
                    data: function () {
                        return {
                            titulo: "Editar menú \"" + menu.nombre + "\"",
                            edicion: true,
                            menu: menu
                        };
                    }
                }
            });
        };

        $scope.borrarMenuModal = function (menu) {
            var modalInstance = $modal.open({
                templateUrl: 'menu/confirm.html',
                controller: 'BorrarMenuModalCtrl',
                resolve: {
                    data: function () {
                        return {
                            titulo: "Eliminar menú \"" + menu.nombre + "\"",
                            menu: menu
                        };
                    }
                }
            });
        };
    }])

.controller('NuevoMenuModalCtrl', ['$scope', '$modalInstance', 'SessionService', 'MenuService', 'toaster', 'data', function ($scope, $modalInstance, SessionService, MenuService, toaster, data) {
        //https://github.com/angular-ui/bootstrap/issues/2110
        $scope.form = {};
        $scope.titulo = data.titulo;
        $scope.edicion = data.edicion;
        $scope.menu = angular.copy(data.menu);
        $scope.categorias = SessionService.getValor(SessionService.CATEGORIAS);
        $scope.imagen = {
            data: ''
        };
        $scope.dirtyAndInvalid = function (o) {
            return o.$dirty && o.$invalid;
        };

        if ($scope.edicion) {
            var casa = SessionService.getValor(SessionService.CASA);
            $scope.imagen.data = 'casas/' + casa.codigoCasa + '/img/viandas/' + $scope.menu.imagen;
        }
        $scope.actualizarImagenACargar = function (imagenBase64) {
            $scope.imagen.data = imagenBase64;
        };
        $scope.borrarImagen = function ($flow) {
            $flow.cancel();
            delete $scope.menu.imagen;

        };

        $scope.existeImagen = function () {
            return (($scope.form.archivo && $scope.form.archivo.files && $scope.form.archivo.files.length > 0) || $scope.menu.imagen);
        };

        $scope.guardar = function() {       
            $scope.guardando = true;
            var menuForm = {
                menu: $scope.menu
            };

            if ($scope.form.archivo && $scope.form.archivo.files && $scope.form.archivo.files.length > 0) {
                menuForm.archivo = $scope.form.archivo.files[0].file;
            }
            if ($scope.edicion) {
                MenuService.actualizar(menuForm.menu, menuForm.archivo).then(
                        function (menuActualizado) {
                            toaster.pop('success', "Menú actualizado con éxito", "Menu: " + menuActualizado.nombre);
                        }, function (error) {
                    toaster.pop('error', "Error al actualizar el menú", "Por favor intente más tarde CODE [" + error.status + "]");
                }).finally(function () {
                    $scope.guardando = false;
                    $modalInstance.close(menuForm);
                });
            } else {
                MenuService.guardar(menuForm.menu, menuForm.archivo).then(
                        function (menuGuardado) {
                            toaster.pop('success', "Menú creado con éxito", "Menu: " + menuGuardado.nombre);
                        }, function (error) {
                    toaster.pop('error', "Error al crear el menu", "Por favor intente más tarde CODE [" + error.status + "]");
                }).finally(function () {
                    $scope.guardando = false;
                    $modalInstance.close(menuForm);
                });
            }
        };
        $scope.cerrar = function () {
            $modalInstance.dismiss('cancelar');
        };

    }])

.controller('BorrarMenuModalCtrl', ['$scope', '$modalInstance', 'MenuService', 'toaster', 'data', function ($scope, $modalInstance, MenuService, toaster, data) {
        $scope.titulo = data.titulo;
        $scope.menu = data.menu;
        $scope.eliminando = false;
        $scope.eliminar = function () {
            $scope.eliminando = true;
            MenuService.borrar($scope.menu).then(
                    function (response) {
                        toaster.pop('success', "Menú borrado con éxito");
                    }, function (error) {
                toaster.pop('error', "Error al borrar el menú", "Por favor intente más tarde CODE [" + error.status + "]");
            }).finally($modalInstance.close());

        };
        $scope.cerrar = function () {
            $modalInstance.dismiss('cancelar');
        };
    }])

.controller('PedidosCtrl', ['$scope', '$routeParams', '$modal', 'NotificacionService', 'PedidoService', 'usSpinnerService', 'toaster', function ($scope, $routeParams, $modal, NotificacionService, PedidoService, usSpinnerService, toaster) {
        $scope.estado = $routeParams.estado;                
        $scope.mostrarError = false;
        usSpinnerService.spin('spinner-1');
        PedidoService.buscarPedidos($scope.estado).then(function (pedidos) {
            $scope.pedidos = pedidos;
            usSpinnerService.stop('spinner-1');
        }, function (error) {
            console.log("Error al visualizar el pedido: " + error);
            $scope.mostrarError = true;
            usSpinnerService.stop('spinner-1');
        });
        $scope.generarComanda = function () {
            $scope.generandoComanda = true;
            NotificacionService.notificarPedidos().then(
                    function (response) {
                        toaster.pop('success', "Comanda generada con éxito");
                    },
                    function (error) {
                        toaster.pop('error', "Error al generar la comanda", "Por favor intente más tarde CODE [" + error.status + "]");
                    }).finally(function () {
                $scope.generandoComanda = false;
            });

        };
        $scope.verDetallePedido = function (pedido) {
            $modal.open({
                templateUrl: 'pedido/detalle.html',
                controller: 'DetallePedidoModalCtrl',
                resolve: {
                    data: function () {
                        return {
                            pedido: pedido
                        };
                    }
                }
            });
        };

    }])

.controller('DetallePedidoModalCtrl', ['$scope', '$modalInstance', 'data', function ($scope, $modalInstance, data) {
        $scope.pedido = data.pedido;
        $scope.cerrar = function () {
            $modalInstance.dismiss('cancelar');
        };
        $scope.calcularCantidadItem = function (pedido) {
            var i, suma = 0;
            for (i = 0; i < pedido.items.length; i++) {
                suma = suma + pedido.items[i].cantidad;
            }
            return suma;
        };
    }])

.controller('ClientesCtrl', ['$scope', '$modal', 'toaster', 'ClienteService', function ($scope, $modal, toaster, ClienteService) {
        ClienteService.buscarClientes().then(                        
                function (clientes) {
                    $scope.clientes = clientes;                                                        
                },
                function (error) {                            
                    $scope.mostrarError = true;
                }).finally(function () {

                });

        $scope.mostrarFicha = function(cliente) {
            var modalInstance = $modal.open({
                templateUrl: 'cliente/ficha.html',                        
                backdrop: 'static',
                controller: 'FichaClienteCtrl',
                resolve: {
                    data: function () {
                        return {
                            cliente: cliente
                        };
                    }
                }
            });                    
        };

    }])

.controller('FichaClienteCtrl', ['$scope', '$modalInstance', 'toaster','ClienteService', 'data', function ($scope, $modalInstance, toaster, ClienteService, data) {
        $scope.cliente = angular.copy(data.cliente);
        $scope.form = {};
        $scope.form.notas = $scope.cliente.notas;
        $scope.cerrar = function () {
            $modalInstance.dismiss('cancelar');
        };

        $scope.guardar = function() {
            $scope.guardandoCliente = true;
            $scope.cliente.notas = $scope.form.notas;
            ClienteService.actualizarNotas($scope.cliente).then(
                    function(response){
                        toaster.pop('success', "Ficha actualizada con éxito");
                    }, function(error){
                        toaster.pop('error', "Error al actualizar la ficha", "Por favor intente más tarde CODE [" + error.status + "]");
                    }).finally(function(){
                        $scope.guardandoCliente = false;
                    });                    
        };

    }]);
