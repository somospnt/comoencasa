'use strict';

/* Controllers */

angular.module('comoEnCasaAdmin.services', [])
        .factory('SessionService', [function () {
                var session = {};
                return {
                    getValor: function (clave) {
                        return session[clave];
                    },
                    setValor: function (clave, valor) {
                        session[clave] = valor;
                    },
                    CASA: "CASA",
                    CATEGORIAS: "CATEGORIAS"
                };
            }])
        .factory('MenuService', ['$http', 'API_REST', function ($http, API_REST) {
                var urlBase = API_REST + '/menus';
                var menus;
                function obtenerFormData(menu, archivo) {
                    var formData = new FormData();
                    formData.append('archivo', archivo);
                    for (var atributo in menu) {
                        if (menu.hasOwnProperty(atributo)) {
                            if (archivo && atributo === 'imagen') {
                                formData.append(atributo, archivo.name);
                            } else {
                                formData.append(atributo, menu[atributo]);
                            }
                        }
                    }


                    return formData;
                }

                function actualizarMenuDeLaLista(menu, menus) {
                    for (var i = 0; i < menus.length; i++) {
                        if (menu.id === menus[i].id) {
                            angular.copy(menu, menus[i]);
                        }
                    }

                }

                function eliminarMenuDeLaLista(menu, menus) {
                    var indice = -1;
                    for (var i = 0; i < menus.length; i++) {
                        if (menu.id === menus[i].id) {
                            indice = i;
                        }
                    }
                    if (indice > -1) {
                        menus.splice(indice, 1);
                    }
                }

                return {
                    getMenus: function () {
                        var promise = $http.get(urlBase).then(function (response) {
                            menus = [];
                            if (Object.prototype.toString.call(response.data) === '[object Array]') {
                                if (response.data.length > 0) {
                                    menus = response.data;
                                }
                            }
                            return menus;
                        });
                        return promise;
                    },
                    guardar: function (menu, file) {
                        var menuForm = {};
                        angular.copy(menu, menuForm);
                        menuForm.categoria = (menu.categoria) ? menu.categoria.id : ""; //Refactor
                        var promise = $http.post(urlBase, obtenerFormData(menuForm, file), {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined}
                        }).then(function (response) {
                            if (file) {
                                menu.imagen = file.name;
                            }
                            menus.push(response.data);
                            return (response.data || menu);
                        });
                        return promise;

                    },
                    actualizar: function (menu, file) {
                        var url = urlBase + '/' + menu.id;
                        var menuForm = {};
                        angular.copy(menu, menuForm);
                        menuForm.categoria = (menu.categoria) ? menu.categoria.id : ""; //Refactor
                        var promise = $http.post(url, obtenerFormData(menuForm, file), {
                            transformRequest: angular.identity,
                            headers: {'Content-Type': undefined}
                        }).then(function (response) {
                            if (file) {
                                menu.imagen = file.name;
                            }
                            actualizarMenuDeLaLista(menu, menus);
                            return (response.data || menu);
                        });
                        return promise;


                    },
                    borrar: function (menu) {
                        var url = urlBase + '/' + menu.id;
                        var promise = $http.delete(url).then(function (response) {
                            eliminarMenuDeLaLista(menu, menus);
                        });
                        return promise;
                    }
                };
            }])
        .factory('NotificacionService', ['$http', 'API_REST', function ($http, API_REST) {
                var urlBase = API_REST + '/notificaciones/pedidos';
                return {
                    notificarPedidos: function (menu, file) {
                        var promise = $http.put(urlBase).then(function (response) {
                            return response.data;
                        });
                        return promise;
                    }
                };
            }])
        .factory('PedidoService', ['$http', 'API_REST', function ($http, API_REST) {
                var urlBase = API_REST + '/pedidos';
                return {
                    buscarPedidos: function (estado) {
                        var urlPeticion = urlBase;
                        if (estado) {
                            urlPeticion = urlPeticion + '?estado=' + estado.toUpperCase();
                        }
                        
                        var promise = $http.get(urlPeticion).then(
                                function (response) {
                                    return response.data;
                                });
                        return promise;
                    }
                };
            }])
        .factory('CategoriaService', ['$http', 'API_REST_PUBLIC', function ($http, API_REST_PUBLIC) {
                var urlBase = API_REST_PUBLIC + '/categorias';
                return {
                    buscarCategorias: function () {
                        var promise = $http.get(urlBase).then(
                                function (response) {
                                    return response.data;
                                });
                        return promise;
                    }
                };
            }])
        .factory('ClienteService', ['$http', 'API_REST', function ($http, API_REST) {
                var urlBase = API_REST + '/clientes';
                var clientes;
                return {
                    buscarClientes: function () {
                        var promise = $http.get(urlBase).then(
                                function (response) {
                                    clientes = response.data;
                                    return clientes;
                                });
                        return promise;

                    },
                    actualizarNotas: function (cliente) {
                        var urlActualizacionNotas = urlBase + '/notas';
                        var promise = $http.put(urlActualizacionNotas, cliente).then(function (response) {
                            if (clientes) {
                                for (var i = 0; i < clientes.length; i++) {
                                    if (clientes[i].id === cliente.id) {
                                        clientes[i] = cliente;
                                    }
                                }
                            }
                            return cliente;
                        });
                        return promise;
                    }

                };
            }]);
