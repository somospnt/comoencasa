'use strict';


angular.module('lib.wrapper', [])

.directive('gmGritterAddOnLoad', [function() {
        return {
            restrict: 'E',
            scope: {
                option: '=option'
            },  
            link: function(scope, element) {                
                var unique_id;
                scope.$watch('option', function(newValue, oldValue) {
                    if (newValue) {
                        if (unique_id) {
                            element.gritter.remove(unique_id);
                            unique_id = null;
                        }
                        unique_id = element.gritter.add(newValue);
                    }
                }, true);
            }
        };
    }])
.directive('gmGritterAddOnClick', [function() {
        return {
            restrict: 'A',
            scope: {
                option: '=option',
                data: '=info'
            },  
            link: function(scope, element) {           
                element.bind('click', function () {
                    var config = angular.extend(scope.data, scope.option);
                    element.gritter.add(config);
                });
            }
        }    
    }])
.directive('gmGritterRemoveAllOnClick', [function() {
        return {
            restrict: 'A',            
            link: function(scope, element) {           
                element.bind('click', function () {                    
                    element.gritter.removeAll();
                });
            }
        }    
    }])
.directive('gmCustomBarChart', [function() {
        return {
            restrict: 'A',            
            link: function(scope, element) {                
                element.find(".bar").each(function(){
                    var $bar = angular.element(this).find(".value");
                    var i = $bar.html();
                    $bar.html("");
                    $bar.animate({height: i}, 2000);
                });
            }
        };
    }])
.directive('gmDragCalendarEvent', [function () {
        return {
            restrict: 'A',
 //           require: 'ngModel',
            link: function (scope, element, attrs) {
                element.draggable({
                    zIndex: 999,
                    revert: true,      // will cause the event to go back to its
                    revertDuration: 0  //  original position after the drag
                });
            }
        };
    }])
.directive('jqSparkline', [function () {
        //https://gist.github.com/pjstarifa/6210002
        'use strict';
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ngModel) {
 
                 var opts={};
                 //TODO: Use $eval to get the object
                opts.type = attrs.type || 'line';
 
                scope.$watch(attrs.ngModel, function () {
                    render();
                });
                
                scope.$watch(attrs.opts, function(){
                  render();
                }
                  );
                var render = function () {
                    var model;
                    if(attrs.opts) angular.extend(opts, angular.fromJson(attrs.opts));
                    console.log(opts);
                    // Trim trailing comma if we are a string
                    angular.isString(ngModel.$viewValue) ? model = ngModel.$viewValue.replace(/(^,)|(,$)/g, "") : model = ngModel.$viewValue;
                    var data;
                    // Make sure we have an array of numbers
                    angular.isArray(model) ? data = model : data = model.split(',');
                    element.sparkline(data, opts);
                };
            }
        };
    }])
.directive("btnLoading", function () {
       return function (scope, element, attrs) {
           scope.$watch(function () {
               return scope.$eval(attrs.ngDisabled);
           }, function (newVal) {
               //you can make the following line more robust
               if (newVal) {
                   return;
               } else {
                   return scope.$watch(function () {
                       return scope.$eval(attrs.btnLoading);
                   },
                   function (loading) {
                       if (loading) return element.button("loading");
                       element.button("reset");
                   });
               }
           });
       };
   });