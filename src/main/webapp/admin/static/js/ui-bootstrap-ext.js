'use strict';


angular.module('ui.bootstrap.ext', ['ui.bootstrap.transition'])

.directive('gmSiderbarCollapse', ['$document', '$transition', function($document, $transition) {

        return {
            link: function(scope, element, attrs) {                
                var principalContentQuerySelector = attrs.gmSiderbarCollapseContent;
                
                var initialAnimSkip = true;
                var currentTransition;
                                
                function doTransition(change) {
                    var newTransition = $transition(element, change);
                    if (currentTransition) {
                        currentTransition.cancel();
                    }
                    currentTransition = newTransition;
                    newTransition.then(newTransitionDone, newTransitionDone);
                    return newTransition;

                    function newTransitionDone() {
                        // Make sure it's this transition, otherwise, leave it alone.
                        if (currentTransition === newTransition) {
                            currentTransition = undefined;
                        }
                    }
                }

                function expand() {
                    var $principalContent = angular.element($document[0].querySelector(principalContentQuerySelector));
                    if (initialAnimSkip) {
                        initialAnimSkip = false;
                        expandDone();
                    } else {
                        element.removeClass('collapse').addClass('collapsing');                        
                        doTransition({'margin-left': '0px'}).then(expandDone);
                        $principalContent.css({'margin-left': element[0].scrollWidth + 'px'});
                    }
                }

                function expandDone() {
                    element.removeClass('collapsing');
                    element.addClass('collapse in');
                }

                function collapse() {
                    var $principalContent = angular.element($document[0].querySelector(principalContentQuerySelector));
                    if (initialAnimSkip) {
                        initialAnimSkip = false;
                        collapseDone();                        
                        element.css({'margin-left': '-' + element[0].scrollWidth + 'px'});
                    } else {
                        element.removeClass('collapse in').addClass('collapsing');
                        doTransition({'margin-left': '-' + element[0].scrollWidth + 'px'}).then(collapseDone);
                    }
                    $principalContent.css({'margin-left':'0px'});
                }

                function collapseDone() {
                    element.removeClass('collapsing');
                    element.addClass('collapse');
                }
                scope.$watch(attrs.gmSiderbarCollapse, function(shouldCollapse) {
                    if (shouldCollapse) {
                        collapse();
                    } else {
                        expand();
                    }
                });
            }
        };
    }]);