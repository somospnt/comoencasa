'use strict';

// Declare app level module which depends on filters, and services
angular.module('comoEnCasaAdmin.config', [])
        .constant('API_REST', 'api/admin')
        .constant('API_REST_PUBLIC', 'api');
