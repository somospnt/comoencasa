'use strict';


// Declare app level module which depends on filters, and services
angular.module('comoEnCasaAdmin', [
  'ngRoute',
  'ui.bootstrap',  
  'ui.bootstrap.ext',    
  'lib.wrapper',
  'flow',
  'toaster',
  'textAngular',
  'angularSpinner',
  'comoEnCasaAdmin.config',
  'comoEnCasaAdmin.directives',
  'comoEnCasaAdmin.services',
  'comoEnCasaAdmin.controllers'  
])
.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/inicio', {templateUrl: 'admin/partials/inicio.htm', controller: 'IndexCtrl'});
  $routeProvider.when('/menus', {templateUrl: 'admin/partials/menus.htm', controller: 'MenusCtrl'});
  $routeProvider.when('/pedidos', {templateUrl: 'admin/partials/pedidos.htm', controller: 'PedidosCtrl'});
  $routeProvider.when('/pedidos/:estado', {templateUrl: 'admin/partials/pedidos.htm', controller: 'PedidosCtrl'});
  $routeProvider.when('/clientes', {templateUrl: 'admin/partials/clientes.htm', controller: 'ClientesCtrl'});
  $routeProvider.otherwise({redirectTo: '/menus'});
}]);