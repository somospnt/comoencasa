'use strict';

/* Controllers */

angular.module('comoEnCasaAdmin.directives', [])
.directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}])
.directive('appFlowImg', [function() {
  //https://github.com/flowjs/ng-flow/issues/37      
  return {
    'scope': false,
    'require': '^flowInit',
    'link': function(scope, element, attrs) {
      var file = attrs.appFlowImg;
      scope.$watch(file, function (file) {
        if (!file) {
          return ;
        }
        var fileReader = new FileReader();
        fileReader.readAsDataURL(file.file);
        fileReader.onload = function (event) {

        // Main part: execute code in `appFlowImgChange` attribute with custom params
          //scope.$apply(attrs.appFlowImgChange, {'$src': event.target.result});
          scope.$apply(function(self) {
            self[attrs.appFlowImgChange](event.target.result);
          });
        };
      });
    }
  };
}]);