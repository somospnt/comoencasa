'use strict';

/* Controllers */

angular.module('tulip.controllers', [])
        .controller('MenuPrincipalCtrl', ['$scope', function ($scope) {
                $scope.seccion = 'carouselSection';
                $scope.setSeccion = function (seccion) {
                    $scope.seccion = seccion;
                };
            }])
        .controller('InicioCtrl', ['$scope', 'CasaService', function ($scope, CasaService) {                
                $scope.init = function(codigoCasa) {
                    $scope.codigoCasa = codigoCasa;
                    CasaService.obtenerCasaPorCodigo($scope.codigoCasa)
                        .success(function (casa) {
                            $scope.casa = casa;
                            CasaService.setCasa($scope.casa);
                            $scope.$broadcast('casa_actualizada', $scope.casa);
                        })
                        .error(function (error) {
                            $scope.status = 'Unable to load customer data: ' + error.message;
                            console.log("Error: " + error.message);
                        });                       
                };
                
            }])
        .controller('ViandaCtrl', ['$scope', 'CarritoService', function ($scope, CarritoService) {
                $scope.$on("casa_actualizada", function (event, casa) {
                    $scope.viandas = casa.menus;
                    angular.forEach($scope.viandas, function (value, key) {
                        value.cantidad = 1;
                    });
                    if ($scope.viandas.length > 0) {
                        $scope.viandaPrincipal = $scope.viandas[0];
                    }
                });
                $scope.mostrarVianda = function (vianda) {
                    $scope.viandaPrincipal = vianda;
                };
                $scope.agregarAlCarrito = function (vianda) {
                    var itemPedido = angular.copy(vianda);
                    CarritoService.agregarVianda(itemPedido);
                };
                $scope.tieneViandas = function () {
                    return CarritoService.tieneViandas();
                };
            }])
        .controller('CarritoCtrl', ['$scope', '$modal', 'CarritoService', function ($scope, $modal, CarritoService) {
                $scope.carrito = CarritoService.obtenerCarrito();
                $scope.eliminiarDelCarrito = function (vianda) {
                    CarritoService.eliminarVianda(vianda);
                };
                $scope.vaciarCarrito = function () {
                    CarritoService.limpiarCarrito();
                };
                $scope.abrirFormularioPedido = function () {
                    var formularioPedidoModal = $modal.open({
                        templateUrl: 'partials/pedidoModal.html',
                        controller: 'FormularioPedidoModalCtrl'
                    });
                    formularioPedidoModal.result.then(function (data) {
                        console.log("oks: [" + data + "]");
                    }, function () {
                        console.log("cancel");
                    });
                };


            }])
        .controller('FormularioPedidoModalCtrl', ['$scope', '$modalInstance', 'CarritoService', 'CasaService', 'PedidoService', 'UsuarioService',
            function ($scope, $modalInstance, CarritoService, CasaService, PedidoService, UsuarioService) {
                $scope.usuarioExiste = false;
                $scope.usuario = {};
                $scope.mensaje = {
                    descripcion: '',
                    tipo: ''
                };
                $scope.cambiarUsuarioExiste = function (valor) {
                    $scope.usuarioExiste = valor;
                    $scope.usuario = {};
                    $scope.mensaje = {
                        descripcion: '',
                        tipo: ''
                    };
                }
                $scope.submitForm = function () {
                    if ($scope.usuario.clave != $scope.usuario.repetirClave) {
                        $scope.mensaje.descripcion = "Las claves ingresadas deben coincidir";
                        $scope.mensaje.tipo = "error";
                        return;
                    }
                    var usuario = {
                        nombre: $scope.usuario.nombre,
                        mail: $scope.usuario.mail,
                        password: $scope.usuario.clave,
                        telefono: $scope.usuario.telefono,
                        direccion: (($scope.usuario.direccion) ? $scope.usuario.direccion : "")
                    };
                    var carrito = CarritoService.obtenerCarrito();
                    carrito.usuario = usuario;
                    carrito.casa = CasaService.getCasa();
                    PedidoService.confirmar(carrito)
                            .success(function () {
                                $scope.mensaje.descripcion = "El pedido se realizó correctamente";
                                $scope.mensaje.tipo = "ok";
                                CarritoService.limpiarCarrito();
                            })
                            .error(function (error) {
                                $scope.mensaje.descripcion = "Error al realizar el pedido, por favor inténtelo más tarde";
                                $scope.mensaje.tipo = "error";
                            });
                };
                $scope.submitUsuario = function () {
                    var usuario = {
                        mail: $scope.usuario.mail,
                        password: $scope.usuario.clave,
                    };
                    var carrito = CarritoService.obtenerCarrito();
                    carrito.usuario = usuario;
                    carrito.casa = CasaService.getCasa();
                    UsuarioService.agregarPedido(carrito)
                        .success(function () {
                            $scope.mensaje.descripcion = "El pedido se realizó correctamente";
                            $scope.mensaje.tipo = "ok";
                            CarritoService.limpiarCarrito();
                        })
                        .error(function (error) {
                            $scope.mensaje.descripcion = "Error al realizar el pedido, por favor inténtelo más tarde";
                            $scope.mensaje.tipo = "error";
                        });
                }
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.tieneMensaje = function () {
                    return ($scope.mensaje && $scope.mensaje.descripcion && ($scope.mensaje.descripcion.trim().length > 0));
                };

            }]);
