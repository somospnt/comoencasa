'use strict';

/* Services */


// Demonstrate how to register services
// In this case it is a simple value service.
angular.module('tulip.services', [])
        .factory('CasaService', ['$http', 'API_REST', function($http, API_REST) {
                var casaActual;
                return {
                    obtenerCasaPorCodigo: function(codigoCasa) {
                        return $http.get(API_REST + '/casas/' + codigoCasa);
                    },
                    getCasa: function() {
                        return casaActual;
                    },
                    setCasa: function(casa) {
                        casaActual = casa;
                    }
                };
            }])
        .factory('CarritoService', ['$http', 'API_REST', function($http, API_REST) {
                var carritoPendiente = {
                    viandas: {}
                };
                return {
                    agregarVianda: function(vianda) {
                        carritoPendiente.viandas[vianda.id] = vianda;
                    },
                    eliminarVianda: function(vianda) {
                        if (carritoPendiente.viandas[vianda.id]) {
                            delete carritoPendiente.viandas[vianda.id];
                        }
                    },
                    obtenerCarrito: function() {
                        return carritoPendiente;
                    },
                    limpiarCarrito: function() {
                        delete carritoPendiente.viandas;
                        carritoPendiente.viandas = {};
                    },
                    tieneViandas: function() {
                        for (var atributo in carritoPendiente.viandas) {
                            return true;
                        }
                        return false;
                    }
                };
            }])
        .factory('PedidoService', ['$http', 'API_REST', function($http, API_REST) {

                return {
                    confirmar: function(carrito) {
                        var pedido = {
                            idCasa: carrito.casa.id,
                            usuario: carrito.usuario,
                            items: []
                        };
                        for (var atributo in carrito.viandas) {        
                            var vianda = carrito.viandas[atributo];
                            var itemPedido = {
                                cantidad: vianda.cantidad,
                                menu: {
                                    id: vianda.id
                                }
                            };                            
                            pedido.items.push(itemPedido);
                        }

                        return $http({
                            url: API_REST + '/pedidos',
                            method: "POST",
                            data: pedido
                        });
                    }
                };
            }])
        .factory('UsuarioService', ['$http', 'API_REST', function($http, API_REST) {

            return {
                agregarPedido: function(carrito) {
                    var pedido = {
                        idCasa: carrito.casa.id,
                        usuario: carrito.usuario,
                        items: []
                    };
                    for (var atributo in carrito.viandas) {
                        var vianda = carrito.viandas[atributo];
                        console.log(vianda);
                        var itemPedido = {
                            cantidad: vianda.cantidad,
                            menu: {
                                id: vianda.id
                            }
                        };
                        pedido.items.push(itemPedido);
                    }

                    return $http({
                        url: API_REST + '/usuarios/pedido',
                        method: "POST",
                        data: pedido
                    });
                }
            };
        }]);
