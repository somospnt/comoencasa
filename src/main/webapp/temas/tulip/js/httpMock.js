'use strict';

(function(ng) {
    console.log('======== MOCK ========');
    initializeStubbedBackend();

    function initializeStubbedBackend() {
        var viandas = [
            {
                id: 1,
                nombre: "Vianda vegana",
                descripcion: "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus dui ligula, vulputate sed commodo non, ultricies sed arcu. Nunc cursus rutrum mattis.",
                imagen: "img/viandas/vianda_1.jpg"
            },
            {
                id: 2,
                nombre: "Chop suey",
                descripcion: "Curabitur auctor semper mi, ut egestas dolor congue et. Nulla tempus sodales elit. Aliquam id condimentum nulla",
                imagen: "img/viandas/vianda_2.jpg"
            },
            {
                id: 3,
                nombre: "Delicias de salmon",
                descripcion: "Proin et scelerisque lectus, in rutrum lacus. Proin nec iaculis metus. Mauris imperdiet risus ante.",
                imagen: "img/viandas/vianda_3.jpg"
            },
            {
                id: 4,
                nombre: "Pan nuestro de cada día",
                descripcion: "Nam lobortis sit amet diam vel sodales. Sed sodales pretium quam sit amet iaculis. Sed magna tellus, rhoncus sed varius nec, molestie nec magna.",
                imagen: "img/viandas/vianda_4.jpg"
            },
            {
                id: 5,
                nombre: "Mazinger bento",
                descripcion: "Phasellus eu facilisis diam, non egestas velit. Suspendisse ultricies aliquet lacinia. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus.",
                imagen: "img/viandas/vianda_5.jpg"
            },
            {
                id: 6,
                nombre: "Salpicon Eco",
                descripcion: "Aliquam ac facilisis arcu, eu tempor justo. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas",
                imagen: "img/viandas/vianda_6.jpg"
            },
            {
                id: 7,
                nombre: "Super Extra Buguer",
                descripcion: "Proin bibendum eget dolor sit amet iaculis. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Etiam posuere mauris libero, non adipiscing magna iaculis in",
                imagen: "img/viandas/vianda_7.jpg"
            },
            {
                id: 8,
                nombre: "A la olla",
                descripcion: "Sed fringilla aliquet nisl, quis sodales orci sagittis ac. Phasellus a augue lorem. Praesent ac sapien enim",
                imagen: "img/viandas/vianda_8.jpg"
            },
            {
                id: 9,
                nombre: "Astroboy bento",
                descripcion: "Nullam et est dui. Curabitur vehicula, sem vitae malesuada commodo, justo neque placerat elit, ut tristique arcu metus bibendum elit.",
                imagen: "img/viandas/vianda_9.jpg"
            }
        ];

        var casaMock = {
                id: "",
                nombre: "",
                codigo: "",
                mail: "",
                descripcion: "",
                imagen: "img/logo.jpg",       
                menus: viandas
        };
        var API_REST_MOCK = 'http://localhost:8080/comoencasa/api';

        ng.module('tulip')
                .config(function($provide) {
                    $provide.decorator('$httpBackend', angular.mock.e2e.$httpBackendDecorator);
                })
                .run(function($httpBackend) {                    
                    $httpBackend.whenGET(/.*api\/[\w-_]*\/menus$/).respond(function(method, url, data) {                                                
                        return [200, viandas, {}];
                    });
                    $httpBackend.whenGET(/.*api\/casas\/[\w-_]*$/).respond(function(method, url, data) {
                        var valores = url.match(/.*api\/casas\/([\w-_]*)$/);
                        casaMock.nombre = "Casa " + valores[1] ;
                        casaMock.codigo = valores[1];
                        return [200, casaMock, {}];
                    });
                    
                    $httpBackend.whenGET(/\/*/).passThrough();
                    $httpBackend.whenPOST(/\/*/).passThrough();
                    $httpBackend.whenPUT(/\/*/).passThrough();                    
                });
    }
})(angular);

/*
 * http://michalostruszka.pl/blog/2013/05/27/easy-stubbing-out-http-in-angularjs-for-backend-less-frontend-development/
 */