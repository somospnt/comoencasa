'use strict';


// Declare app level module which depends on filters, and services
var comoencasa = angular.module('casaApp', [
    'ngRoute',
    'tulip.config',
    'tulip.filters',
    'tulip.services',
    'tulip.controllers',
    'ui.bootstrap'
]);

