'use strict';

// Declare app level module which depends on filters, and services
angular.module('tulip.config', [])
   .constant('API_REST', 'api');
