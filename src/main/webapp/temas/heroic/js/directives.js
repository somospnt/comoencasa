'use strict';

angular.module('tulip.directives', [])
   .directive('cbTabs', function($document) {
        return {
            restrict: 'A',
            link: function(scope, element, attrs) { 
                var contenidoQuerySelector = attrs.cbTabsContent;
                element.tabs(contenidoQuerySelector, {click: function(el){
                        console.log("click" + el);
                }});
            }
        };
});
