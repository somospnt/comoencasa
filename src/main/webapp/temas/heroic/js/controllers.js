'use strict';

/* Controllers */

angular.module('tulip.controllers', [])
        .controller('MenuPrincipalCtrl', ['$scope', function ($scope) {
                $scope.seccion = 'carouselSection';
                $scope.setSeccion = function (seccion) {
                    $scope.seccion = seccion;
                };
            }])
        .controller('InicioCtrl', ['$scope', 'CasaService', 'CarritoService', function ($scope, CasaService, CarritoService) {
                $scope.init = function (codigoCasa) {
                    $scope.codigoCasa = codigoCasa;
                    $scope.cantidadViandas = function(){
                        return CarritoService.cantidadViandas();
                    };
                    CasaService.obtenerCasaPorCodigo($scope.codigoCasa)
                            .success(function (casa) {
                                $scope.casa = casa;
                                CasaService.setCasa($scope.casa);
                                $scope.$broadcast('casa_actualizada', $scope.casa);
                            })
                            .error(function (error) {
                                $scope.status = 'Unable to load customer data: ' + error.message;
                                console.log("Error: " + error.message);
                            });

                };

            }])
        .controller('ViandaCtrl', ['$scope', '$modal', 'CarritoService', 'CategoriaService', function ($scope, $modal, CarritoService, CategoriaService) {
                $scope.categorias = [];
                $scope.categoriaActual = {};

                function marcarCategoriasExistentes(categorias, viandas) {
                    angular.forEach(categorias, function (categoria, key) {
                        categoria.empty = true;
                        angular.forEach(viandas, function (vianda, key) {
                            if(!vianda.categoria && categoria.id === 0) {                                
                                categoria.empty = false;                                
                            } else if (vianda.categoria) {
                                if (vianda.categoria.id === categoria.id) {
                                    categoria.empty = false;
                                }
                            }
                        });                        
                    });
                }
                function obtenerPrimeraCategoriaConVianda(categorias) {                   
                    for(var i=0; i<categorias.length; i++) {
                        if(!categorias[i].empty) {
                            return categorias[i];
                        }
                    }
                }
                
                function obtenerViandasVisibles(viandas) {
                    var viandasFiltradas = new Array();
                    angular.forEach(viandas, function (vianda, key) {
                        if (vianda.visible) {
                            viandasFiltradas.push(vianda);
                        }
                    });
                    return viandasFiltradas;
                }

                CategoriaService.buscarCategorias().then(
                        function (categorias) {
                            $scope.categorias = categorias;
                            $scope.categorias.push({
                                id: 0,
                                nombre: "Otros"
                            });
                            $scope.categoriaActual = $scope.categorias[0];
                            if ($scope.viandas) {
                                marcarCategoriasExistentes($scope.categorias, $scope.viandas);
                                $scope.cambiarCategoria(obtenerPrimeraCategoriaConVianda($scope.categorias));
                            }

                        },
                        function (error) {
                            console.log("Error al obtener las categorias: " + error);
                        });

                $scope.$on("casa_actualizada", function (event, casa) {
                    $scope.viandas = obtenerViandasVisibles(casa.menus);
                    angular.forEach($scope.viandas, function (value, key) {
                        value.cantidad = 1;
                    });
                    if ($scope.viandas.length > 0) {
                        $scope.viandaPrincipal = $scope.viandas[0];
                    }
                    if ($scope.categorias) {
                        marcarCategoriasExistentes($scope.categorias, $scope.viandas);
                        $scope.cambiarCategoria(obtenerPrimeraCategoriaConVianda($scope.categorias));
                    }
                });

                $scope.filtroCategoria = function (item) {
                    if (!$scope.categoriaActual) {
                        return false;
                    }
                    if (item.categoria) {
                        return ($scope.categoriaActual.id === item.categoria.id);
                    }
                    return ($scope.categoriaActual.id === 0);
                };
                $scope.agregarAlCarrito = function (vianda) {
                    var itemPedido = angular.copy(vianda);
                    CarritoService.agregarVianda(itemPedido);
                };
                $scope.tieneViandas = function () {
                    return CarritoService.tieneViandas();
                };
                $scope.cambiarCategoria = function (categoria) {
                    $scope.categoriaActual = categoria;
                };
                $scope.esCategoriaSelecccionada = function (categoria) {
                    return ($scope.categoriaActual === categoria);
                };
                $scope.mostrarVianda = function (vianda) {
                    $modal.open({
                        templateUrl: 'partials/viandaModal.html',
                        controller: 'ViandaModalCtrl',
                        size: 'lg',
                        resolve: {
                            parametros: function () {
                                return {
                                    vianda: vianda,
                                    codigoCasa: $scope.codigoCasa
                                };
                            }
                        }
                    });
                };
            }])
        .controller('ViandaModalCtrl', ['$scope', '$modalInstance', 'parametros', function ($scope, $modalInstance, parametros) {
                $scope.vianda = parametros.vianda;
                $scope.codigoCasa = parametros.codigoCasa;
                $scope.cerrar = function() {
                   $modalInstance.dismiss('cancel');
                };
            }])
        .controller('CarritoCtrl', ['$scope', '$modal', 'CarritoService', function ($scope, $modal, CarritoService) {
                $scope.carrito = CarritoService.obtenerCarrito();
                $scope.tieneViandas = function() {
                    return CarritoService.tieneViandas();
                };
        
                $scope.eliminiarDelCarrito = function (vianda) {
                    CarritoService.eliminarVianda(vianda);
                };
                $scope.vaciarCarrito = function () {
                    CarritoService.limpiarCarrito();
                };
                $scope.abrirFormularioPedido = function () {
                    var formularioPedidoModal = $modal.open({
                        templateUrl: 'partials/pedidoModal.html',
                        controller: 'FormularioPedidoModalCtrl'
                    });
                    formularioPedidoModal.result.then(function (data) {
                        console.log("oks: [" + data + "]");
                    }, function () {
                        console.log("cancel");
                    });
                };


            }])
        .controller('FormularioPedidoModalCtrl', ['$scope', '$modalInstance', 'CarritoService', 'CasaService', 'PedidoService', 'UsuarioService',
            function ($scope, $modalInstance, CarritoService, CasaService, PedidoService, UsuarioService) {
                $scope.usuarioExiste = false;
                $scope.usuario = {};
                $scope.mensaje = {
                    descripcion: '',
                    tipo: ''
                };
                $scope.cambiarUsuarioExiste = function (valor) {
                    $scope.usuarioExiste = valor;
                    $scope.usuario = {};
                    $scope.mensaje = {
                        descripcion: '',
                        tipo: ''
                    };
                }
                $scope.submitForm = function () {
                    if ($scope.usuario.clave != $scope.usuario.repetirClave) {
                        $scope.mensaje.descripcion = "Las claves ingresadas deben coincidir";
                        $scope.mensaje.tipo = "error";
                        return;
                    }
                    var usuario = {
                        nombre: $scope.usuario.nombre,
                        mail: $scope.usuario.mail,
                        password: $scope.usuario.clave,
                        telefono: $scope.usuario.telefono,
                        direccion: (($scope.usuario.direccion) ? $scope.usuario.direccion : "")
                    };
                    var carrito = CarritoService.obtenerCarrito();
                    carrito.usuario = usuario;
                    carrito.casa = CasaService.getCasa();
                    PedidoService.confirmar(carrito)
                            .success(function () {
                                $scope.mensaje.descripcion = "El pedido se realizó correctamente";
                                $scope.mensaje.tipo = "ok";
                                CarritoService.limpiarCarrito();
                            })
                            .error(function (error) {
                                $scope.mensaje.descripcion = "Error al realizar el pedido, por favor inténtelo más tarde";
                                $scope.mensaje.tipo = "error";
                            });
                };
                $scope.submitUsuario = function () {
                    var usuario = {
                        mail: $scope.usuario.mail,
                        password: $scope.usuario.clave,
                    };
                    var carrito = CarritoService.obtenerCarrito();
                    carrito.usuario = usuario;
                    carrito.casa = CasaService.getCasa();
                    UsuarioService.agregarPedido(carrito)
                            .success(function () {
                                $scope.mensaje.descripcion = "El pedido se realizó correctamente";
                                $scope.mensaje.tipo = "ok";
                                CarritoService.limpiarCarrito();
                            })
                            .error(function (error) {
                                $scope.mensaje.descripcion = "Error al realizar el pedido, por favor inténtelo más tarde";
                                $scope.mensaje.tipo = "error";
                            });
                };
                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };
                $scope.tieneMensaje = function () {
                    return ($scope.mensaje && $scope.mensaje.descripcion && ($scope.mensaje.descripcion.trim().length > 0));
                };

            }]);
