DROP TABLE IF EXISTS cec_cliente;
DROP TABLE IF EXISTS cec_rol;
DROP TABLE IF EXISTS cec_pedido_item;
DROP TABLE IF EXISTS cec_pedido;
DROP TABLE IF EXISTS cec_usuario;
DROP TABLE IF EXISTS cec_menu;
DROP TABLE IF EXISTS cec_casa;
DROP TABLE IF EXISTS cec_categoria;

CREATE TABLE IF NOT EXISTS cec_casa (
  id          BIGINT IDENTITY PRIMARY KEY,
  nombre      VARCHAR(255) NOT NULL,
  codigo      VARCHAR(255) NOT NULL,
  mail        VARCHAR(255) DEFAULT NULL,
  descripcion VARCHAR(255),
  imagen      VARCHAR(255) DEFAULT NULL
);

CREATE TABLE IF NOT EXISTS cec_categoria (
  id          BIGINT IDENTITY PRIMARY KEY,
  nombre      VARCHAR(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS cec_menu (
  id          BIGINT IDENTITY PRIMARY KEY,
  nombre      VARCHAR(255) NOT NULL,
  descripcion VARCHAR(255),
  id_casa     BIGINT       NOT NULL,
  id_categoria BIGINT,
  imagen      VARCHAR(255) DEFAULT NULL,
  visible     BIT DEFAULT 1
);

CREATE TABLE IF NOT EXISTS cec_pedido (
  id         BIGINT IDENTITY PRIMARY KEY,
  fecha      DATE         NOT NULL,
  id_casa    BIGINT       NOT NULL,
  id_usuario BIGINT       NOT NULL,
  estado     VARCHAR(100) NOT NULL
);

CREATE TABLE IF NOT EXISTS cec_pedido_item (
  id        BIGINT IDENTITY PRIMARY KEY,
  id_menu   BIGINT NOT NULL,
  id_pedido BIGINT NOT NULL,
  cantidad  BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS cec_usuario (
  id        BIGINT IDENTITY PRIMARY KEY,
  id_casa   BIGINT,
  nombre    VARCHAR(255) NOT NULL,
  direccion VARCHAR(255) DEFAULT NULL,
  telefono  VARCHAR(255) DEFAULT NULL,
  password  VARCHAR(255) NULL,
  enabled   BOOLEAN DEFAULT TRUE,
  mail      VARCHAR(255) NOT NULL,
  FOREIGN KEY (id_casa) REFERENCES cec_casa (id)
);

CREATE TABLE cec_rol (
  id         BIGINT IDENTITY PRIMARY KEY,
  id_usuario BIGINT       NOT NULL,
  rol        VARCHAR(255) NOT NULL,
  FOREIGN KEY (id_usuario) REFERENCES cec_usuario (id),
  UNIQUE (id_usuario, rol)
);

ALTER TABLE cec_menu
ADD CONSTRAINT cec_menu_ibfk_1 FOREIGN KEY (id_casa) REFERENCES cec_casa (id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE cec_menu
ADD CONSTRAINT cec_menu_ibfk_2 FOREIGN KEY (id_categoria) REFERENCES cec_categoria (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE cec_pedido
ADD CONSTRAINT cec_pedido_ibfk_1 FOREIGN KEY (id_casa) REFERENCES cec_casa (id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE cec_pedido
ADD CONSTRAINT cec_pedido_ibfk_2 FOREIGN KEY (id_usuario) REFERENCES cec_usuario (id) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE cec_pedido_item
ADD CONSTRAINT cec_pedido_item_ibfk_1 FOREIGN KEY (id_menu) REFERENCES cec_menu (id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE cec_pedido_item
ADD CONSTRAINT cec_pedido_item_ibfk_2 FOREIGN KEY (id_pedido) REFERENCES cec_pedido (id) ON DELETE CASCADE ON UPDATE CASCADE;


ALTER TABLE cec_casa ADD UNIQUE (
  codigo
);

ALTER TABLE cec_usuario ADD UNIQUE (
  mail
);

INSERT INTO cec_categoria (id, nombre) VALUES
(1, 'Carnes'),
(2, 'Pollos'),
(3, 'Pescados'),
(4, 'Pastas'),
(5, 'Vegetarianas'),
(6, 'Tartas'),
(7, 'Meriendas');

INSERT INTO cec_casa (
  id,
  nombre,
  codigo,
  mail,
  descripcion,
  imagen
)
VALUES (
  1, 'KalCero', 'kalcero', 'info@kalcero.com', NULL, 'kalcero.jpg'
), (
2, 'Ratatouille', 'ratatouille', 'info@ratatouille.com', NULL , 'ratatouille.jpg'
), (
3, 'TEST-PEDIDO', 'Casa para realizar pruebas sobre los pedidos', 'info@pedido.com', NULL , 'imagen.jpg'
);


INSERT INTO cec_menu (
  id,
  nombre,
  descripcion,
  id_casa,
  id_categoria,
  imagen
)
VALUES (
  1, 'Vianda vegana', 'Vianda vegana descripciÃ³n.', 1,5, 'vianda_vegana.jpg'
), (
  2, 'Vianda oriental con pollo', 'Vianda oriental descripcion.', 1,2, 'vianda_oriental.jpg'
), (
  3, 'Vianda saludable', 'Vianda saludable descripciÃ³n.', 1, NULL, 'vianda_saludable.jpg'
), (
  4, 'Vianda promo', 'Vianda promo descripciÃ³n.', 2, NULL, 'vianda_promo.jpg'
), (
  5, 'Vianda pan de cada dia', 'Comete la flautita', 2, NULL, 'vianda_promo2.jpg'
), (
  6, 'Vianda pan de cada dia', 'Comete la flautita', 1, NULL, NULL
), (
7, 'Menu de pedido Test', 'Descripcion del pedido Test', 3, NULL, NULL
);

INSERT INTO cec_usuario (
  id,
  nombre,
  direccion,
  telefono,
  mail,
  password
)
VALUES (
  1, 'Maro', 'Calle Falsa 123', '123456789', 'maro@mail.com', '$2a$10$i4/YJRFcZ3txKAqzktDrkubAj9FhlFGoDcjN2n5z5MwpQl/CDbS2u'
);
INSERT INTO cec_usuario (
  id,
  id_casa,
  nombre,
  direccion,
  telefono,
  mail,
  password
)
VALUES (
  2, 1, 'usuarioTest', 'Calle Falsa 123', '123456789', 'usuarioTest@mail.com', '$2a$10$i4/YJRFcZ3txKAqzktDrkubAj9FhlFGoDcjN2n5z5MwpQl/CDbS2u'
);

INSERT INTO cec_usuario (
  id,
  id_casa,
  nombre,
  direccion,
  telefono,
  mail,
  password
)
VALUES (
  3, 2, 'adminRatatouille', 'Calle True 123', '123456789', 'ratatouille@mail.com', '$2a$10$i4/YJRFcZ3txKAqzktDrkubAj9FhlFGoDcjN2n5z5MwpQl/CDbS2u'
);

INSERT INTO cec_usuario 
(id, id_casa, nombre, direccion, telefono, mail, password)
VALUES 
(4, 3, 'pedidoTest', 'Calle Falsa 123', '123456789', 'pedidoTest@mail.com', '$2a$10$i4/YJRFcZ3txKAqzktDrkubAj9FhlFGoDcjN2n5z5MwpQl/CDbS2u');
INSERT INTO cec_rol (
  id,
  id_usuario,
  rol
)
VALUES (
  1, 1, 'ROL_ADMIN'
);

INSERT INTO cec_rol (
  id,
  id_usuario,
  rol
)
VALUES (
  2, 2, 'ROLE_CASA'
);

INSERT INTO cec_rol (
  id,
  id_usuario,
  rol
)
VALUES (
  3, 3, 'ROLE_CASA'
);
INSERT INTO cec_rol (
  id,
  id_usuario,
  rol
)
VALUES (
  4, 4, 'ROLE_CASA'
);

INSERT INTO cec_pedido 
(id,fecha,id_casa,id_usuario,estado)
VALUES 
(1, now(), 1, 1, 'PENDIENTE'), 
(2, now(), 1, 1, 'PENDIENTE'),
(3, dateadd('day', -28, CURRENT_DATE), 3, 1, 'PENDIENTE'),
(4, now(), 3, 2, 'PENDIENTE'),
(5, dateadd('day', -30, CURRENT_DATE), 3, 1, 'INFORMADO');

INSERT INTO cec_pedido_item 
(id,id_menu,id_pedido,cantidad)
VALUES
(1,1,1,10),
(2,2,1,20),
(3,4,1,30),
(4,7,3,10),
(5,7,4,20),
(6,7,3,30);


CREATE TABLE cec_cliente (
  id         BIGINT IDENTITY NOT NULL,
  id_casa    BIGINT          NOT NULL,
  id_usuario BIGINT          NOT NULL,
  notas      VARCHAR(255),
  fecha_alta TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
  UNIQUE (id_casa, id_usuario)
);

ALTER TABLE cec_cliente
ADD CONSTRAINT cec_cliente_ibfk_1 FOREIGN KEY (id_casa) REFERENCES cec_casa (id) ON DELETE CASCADE ON UPDATE CASCADE;
ALTER TABLE cec_cliente
ADD CONSTRAINT cec_cliente_ibfk_2 FOREIGN KEY (id_usuario) REFERENCES cec_usuario (id) ON DELETE CASCADE ON UPDATE CASCADE;

INSERT INTO cec_cliente
(id, id_casa, id_usuario, notas, fecha_alta)
VALUES
  (1, 2, 2, 'una nota copada', CURRENT_TIMESTAMP),
  (2, 2, 1, 'una nota inutil', CURRENT_TIMESTAMP),
  (3, 1, 2, 'una nota util', CURRENT_TIMESTAMP);
