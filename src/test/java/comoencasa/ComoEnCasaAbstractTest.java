package comoencasa;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.sql.DataSource;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationConfig.class)
@WebAppConfiguration
@ActiveProfiles("test")
public abstract class ComoEnCasaAbstractTest {
    
    @Autowired
    private DataSource dataSource;

    protected JdbcTemplate jdbcTemplate;
    
    @Before
    public void prepareDatabaseConnection() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @After
    public void logout() {
        SecurityContextHolder.clearContext();
    }
    
    private void login(String username, String password) {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken(username, password);
        SecurityContextHolder.getContext().setAuthentication(token);
    }

    protected void login(String username) {
        login(username, "123456");
    }
    
    public void loginAdministradorDeCasaKalcero() {
        login("usuarioTest@mail.com", "123456");
    }
    public void loginRolObsoleto() {
        login("maro@mail.com", "123456");
    }
}
