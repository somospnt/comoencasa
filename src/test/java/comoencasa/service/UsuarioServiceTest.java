package comoencasa.service;

import comoencasa.ApplicationConfig;
import comoencasa.domain.Pedido;
import comoencasa.domain.Usuario;
import comoencasa.util.PedidoBuilder;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.sql.DataSource;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationConfig.class)
@ActiveProfiles("test")
@WebAppConfiguration
public class UsuarioServiceTest {

    @Autowired
    private UsuarioService instancia;

    @Autowired
    private DataSource dataSource;

    protected JdbcTemplate jdbcTemplate;

    @Before
    public void init() {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @After
    public void tearDown() {

        SecurityContextHolder.getContext().setAuthentication(null);
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void obtenerUsuarioLogueado_usuarioNoLogueado_lanzaExcepcionDeSeguridad() {
        instancia.obtenerUsuarioLogueado();
    }

    @Test
    public void obtenerUsuarioLogueado_usuarioLogueado_devuelveUsuario() {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("usuarioTest@mail.com", "123456");
        SecurityContextHolder.getContext().setAuthentication(token);

        Usuario usuario = instancia.obtenerUsuarioLogueado();

        assertNotNull(usuario);
        assertEquals("usuarioTest", usuario.getNombre());
    }

    @Test
    public void obtenerUsuarioLogueado_usuarioLogueadoConCasa_devuelveUsuarioConCasa() {
        UsernamePasswordAuthenticationToken token = new UsernamePasswordAuthenticationToken("usuarioTest@mail.com", "123456");
        SecurityContextHolder.getContext().setAuthentication(token);

        Usuario usuario = instancia.obtenerUsuarioLogueado();

        assertNotNull(usuario);
        assertNotNull(usuario.getCasa());
        assertEquals(1L, usuario.getCasa().getId().longValue());
    }

    @Test
    public void validarUsuario_conUsuarioExistenteYPasswordCorrecto_retornaUsuarioHidratado() {
        //setUp
        Usuario usuario = new Usuario();
        usuario.setMail("maro@mail.com");
        usuario.setPassword("123456");

        //ejercitamos
        Usuario resultado = instancia.validarUsuario(usuario);

        //verificamos
        Assert.assertNotNull(resultado);
        Assert.assertNotNull(resultado.getDireccion());
        Assert.assertNotNull(resultado.getMail());
        Assert.assertNotNull(resultado.getNombre());
    }

    @Test(expected = BadCredentialsException.class)
    public void validarUsuario_conUsuarioExistenteYPasswordIncorrecto_lanzaAuthenticationExceptionException() {
        //setUp
        Usuario usuario = new Usuario();
        usuario.setMail("maro@mail.com");
        usuario.setPassword("PasswordInvalido");

        //ejercitamos
        instancia.validarUsuario(usuario);
    }

    @Test(expected = UsernameNotFoundException.class)
    public void validarUsuario_conUsuarioInexistente_lanzaUsernameNotFoundExceptionException() {
        //setUp
        Usuario usuario = new Usuario();
        usuario.setMail("NoExiste@mail.com");
        usuario.setPassword("123456");

        //ejercitamos
        instancia.validarUsuario(usuario);

    }

    @Test
    public void guardar_conUsuarioInexistente_guardaUsuarioConClaveEncriptada() {
        //setUp
        Usuario usuario = new Usuario();
        usuario.setNombre("Hola");
        usuario.setPassword("mundo");
        usuario.setDireccion("calle falsa 123");
        usuario.setMail("UnMailNuevo@mail.com");
        usuario.setPassword(String.valueOf(Math.random() * 1000));

        String sqlUsuarios = "SELECT COUNT(*) FROM cec_usuario";
        String sqlUsuariosConClaveSinEncriptar = "SELECT COUNT(*) FROM cec_usuario where password = '" + usuario.getPassword() + "'";

        Integer cantidadUsuariosAntes = jdbcTemplate.queryForObject(sqlUsuarios, Integer.class);
        Integer cantidadUsuariosConClaveSinEncriptarAntes = jdbcTemplate.queryForObject(sqlUsuariosConClaveSinEncriptar, Integer.class);

        //ejercitamos
        Usuario ressultado = instancia.guardar(usuario);

        //post SetUp
        Integer cantidadUsuariosDespues = jdbcTemplate.queryForObject(sqlUsuarios, Integer.class);
        Integer cantidadUsuariosConClaveSinEncriptarDespues = jdbcTemplate.queryForObject(sqlUsuariosConClaveSinEncriptar, Integer.class);


        //verificamos
        Assert.assertNotNull(ressultado);
        Assert.assertNotNull(ressultado.getId());
        Assert.assertEquals(cantidadUsuariosAntes +1 , cantidadUsuariosDespues.intValue());
        Assert.assertEquals(cantidadUsuariosConClaveSinEncriptarAntes , cantidadUsuariosConClaveSinEncriptarDespues);
    }


}
