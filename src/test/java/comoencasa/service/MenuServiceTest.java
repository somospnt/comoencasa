package comoencasa.service;

import comoencasa.ComoEnCasaAbstractTest;
import comoencasa.domain.Casa;
import comoencasa.domain.Menu;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.After;

import static org.junit.Assert.*;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.annotation.DirtiesContext;


public class MenuServiceTest extends ComoEnCasaAbstractTest {

    @Autowired
    private MenuService instancia;

    @Value("${comoencasa.casa.directorio}")
    private String pathDirectorioCasa;

    @Value("${comoencasa.casa.archivos.imagen.menu}")
    private String pathImagenMenu;

    @After
    public void tearDown() throws IOException {
        File directorioADeshabilitar = new File(pathDirectorioCasa);
        directorioADeshabilitar.setWritable(true);

        File directorio = new File(pathDirectorioCasa);
        FileUtils.deleteDirectory(directorio);
    }

    @Test
    public void buscar_conCodigoCasaValido_retornaListaMenusConTresMenusExistentes() {
        //setUp
        String codigo = "kalcero";
        int cantidadEsperada = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM cec_menu where id_casa=1", Integer.class);

        //ejercitamos
        List<Menu> resultado = instancia.buscar(codigo);


        //ratatouille
        //verificamos
        assertNotNull(resultado);
        assertEquals(cantidadEsperada, resultado.size());
    }

    @Test
    public void buscar_conCodigoCasaInvalido_retornaListaVacia() {
        //setUp
        String codigo = "casa-no-existe";

        //ejercitamos
        List<Menu> resultado = instancia.buscar(codigo);

        //ratatouille
        //verificamos
        assertNotNull(resultado);
        assertEquals(0, resultado.size());
    }

    @Test
    public void guardar_MenuValidoConTodosLosCampos_retornaMenuConId() {
        InputStream inputStream = getClass().getResourceAsStream("/img/vianda_1.jpg");
        Casa casa = new Casa();
        casa.setId(1L);
        casa.setCodigo("kalcero");

        Menu menu = new Menu();
        menu.setCasa(casa);
        menu.setNombre("Menu 1");
        menu.setDescripcion("Una descripcion");
        menu.setImagen("imagen.jpg");
        int filasAntes = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM cec_menu", Integer.class);

        loginAdministradorDeCasaKalcero();
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        Menu menuGuardado = instancia.guardar(menu, inputStream);

        int filasDespues = jdbcTemplate.queryForObject("SELECT COUNT(*) FROM cec_menu", Integer.class);

        assertNotNull(menuGuardado);
        assertNotNull(menuGuardado.getId());
        assertEquals(menu.getDescripcion(), menuGuardado.getDescripcion());
        assertEquals(menu.getNombre(), menuGuardado.getNombre());
        assertEquals(menu.getImagen(), menuGuardado.getImagen());
        assertEquals(filasAntes + 1, filasDespues);

        File imagenGuardada = obtenerArchivoImagenDelMenu(casa.getCodigo(), menu.getImagen());
        assertTrue("No se creo la imagen del menu", imagenGuardada.isFile());
    }

    @Test
    public void guardar_MenuValidoConInputStreamNull_retornaMenuSinNombreDeImagen() {
        Casa casa = new Casa();
        casa.setId(1L);
        casa.setCodigo("kalcero");

        Menu menu = new Menu();
        menu.setCasa(casa);
        menu.setNombre("Menu 1");
        menu.setDescripcion("Una descripcion");
        menu.setImagen("imagen.jpg");

        loginAdministradorDeCasaKalcero();
        Menu menuGuardado = instancia.guardar(menu, null);

        assertNotNull(menuGuardado);
        assertNotNull(menuGuardado.getId());
        assertNull(menu.getImagen());


        File imagenGuardada = obtenerArchivoImagenDelMenu(casa.getCodigo(), menu.getImagen());
        assertFalse("No se tiene que persistir la imagen", imagenGuardada.isFile());
    }

    @Test
    public void guardar_MenuValidoConErrorAlGuardarLaImagen_retornaMenuSinNombreDeImagen() throws IOException {
        InputStream inputStream = getClass().getResourceAsStream("/img/vianda_1.jpg");
        Casa casa = new Casa();
        casa.setId(1L);
        casa.setCodigo("kalcero");

        Menu menu = new Menu();
        menu.setCasa(casa);
        menu.setNombre("Menu 1");
        menu.setDescripcion("Una descripcion");
        menu.setImagen("imagen.jpg");

        loginAdministradorDeCasaKalcero();
        inputStream.close();
        Menu menuGuardado = instancia.guardar(menu, inputStream);

        assertNotNull(menuGuardado);
        assertNotNull(menuGuardado.getId());
        assertNull(menu.getImagen());

        File imagenGuardada = obtenerArchivoImagenDelMenu(casa.getCodigo(), menu.getImagen());
        assertFalse("No se tiene que persistir la imagen", imagenGuardada.isFile());
    }


    @Test
    public void actualizar_MenuValido_retornaMenuActualizado() throws IOException {
        InputStream inputStream = getClass().getResourceAsStream("/img/vianda_1.jpg");
        Long idMenu = 1L;
        Casa casa = new Casa();
        casa.setId(1L);
        casa.setCodigo("kalcero");

        Menu menu = new Menu();
        menu.setCasa(casa);
        menu.setId(idMenu);
        menu.setNombre("Menu 1");
        menu.setDescripcion("Una descripcion");
        menu.setImagen("imagen.jpg");

        loginAdministradorDeCasaKalcero();
        instancia.actualizar(menu, inputStream);

        Menu menuGuardado = obtenerMenuPorId(idMenu);
        assertEquals(menu.getDescripcion(), menuGuardado.getDescripcion());
        assertEquals(menu.getNombre(), menuGuardado.getNombre());
        assertEquals(menu.getImagen(), menuGuardado.getImagen());

        File imagenGuardada = obtenerArchivoImagenDelMenu(casa.getCodigo(), menu.getImagen());

        assertTrue("La imagen no se creo correctamente", imagenGuardada.isFile());
    }

    @Test
    public void actualizar_MenuValidoConNuevaImagen_retornaMenuActualizadoYEliminaImagenAnterior() throws IOException {
        InputStream inputStream = getClass().getResourceAsStream("/img/vianda_1.jpg");
        InputStream inputStreamTest = getClass().getResourceAsStream("/img/vianda_1.jpg");
        Long idMenu = 1L;
        Casa casa = new Casa();
        casa.setId(1L);
        casa.setCodigo("kalcero");

        Menu menu = new Menu();
        menu.setCasa(casa);
        menu.setId(idMenu);
        menu.setNombre("Menu 2");
        menu.setDescripcion("Una descripcion");
        menu.setImagen("imagen2.jpg");
        Menu menuOriginal = obtenerMenuPorId(idMenu);
        File imagenOriginal = obtenerArchivoImagenDelMenu(casa.getCodigo(), menuOriginal.getImagen());
        //Creo el archivo de prueba
        FileUtils.copyInputStreamToFile(inputStreamTest, imagenOriginal);

        loginAdministradorDeCasaKalcero();
        instancia.actualizar(menu, inputStream);

        Menu menuGuardado = obtenerMenuPorId(idMenu);
        assertEquals(menu.getImagen(), menuGuardado.getImagen());
        File imagenGuardada = obtenerArchivoImagenDelMenu(casa.getCodigo(), menu.getImagen());
        assertTrue("La imagen nos se creo correctamente", imagenGuardada.isFile());
        assertFalse("La imange no se borro correctamente", imagenOriginal.isFile());
    }

    @Test
    public void actualizar_MenuValidoSinNombreDeImagenEInputStreamNulo_retornaMenuActualizadoYEliminaImagenAnterior() throws IOException {
        InputStream inputStreamTest = getClass().getResourceAsStream("/img/vianda_1.jpg");
        Long idMenu = 1L;
        Casa casa = new Casa();
        casa.setId(1L);
        casa.setCodigo("kalcero");

        Menu menu = new Menu();
        menu.setCasa(casa);
        menu.setId(idMenu);
        menu.setNombre("Menu 2");
        menu.setDescripcion("Una descripcion");
        menu.setImagen(null);
        Menu menuOriginal = obtenerMenuPorId(idMenu);
        File imagenOriginal = obtenerArchivoImagenDelMenu(casa.getCodigo(), menuOriginal.getImagen());
        //Creo el archivo de prueba
        FileUtils.copyInputStreamToFile(inputStreamTest, imagenOriginal);

        loginAdministradorDeCasaKalcero();
        instancia.actualizar(menu, null);

        Menu menuGuardado = obtenerMenuPorId(idMenu);
        assertNull(menuGuardado.getImagen());
        assertFalse("La imange no se borro correctamente", imagenOriginal.isFile());
    }

    @Test
    public void actualizar_MenuValidoConElInputStreamEnNull_retornaMenuActualizadoYNoModificaLaImagen() throws IOException {
        InputStream inputStreamTest = getClass().getResourceAsStream("/img/vianda_1.jpg");
        Long idMenu = 1L;
        Casa casa = new Casa();
        casa.setId(1L);
        casa.setCodigo("kalcero");

        Menu menu = new Menu();
        menu.setCasa(casa);
        menu.setId(idMenu);
        menu.setNombre("Menu 2");
        menu.setDescripcion("Una descripcion");
        menu.setImagen("imagen falsa");
        Menu menuOriginal = obtenerMenuPorId(idMenu);
        File imagenOriginal = obtenerArchivoImagenDelMenu(casa.getCodigo(), menuOriginal.getImagen());
        //Creo el archivo de prueba
        FileUtils.copyInputStreamToFile(inputStreamTest, imagenOriginal);

        loginAdministradorDeCasaKalcero();
        instancia.actualizar(menu, null);

        Menu menuGuardado = obtenerMenuPorId(idMenu);
        assertEquals(menuOriginal.getImagen(), menuGuardado.getImagen());
        assertTrue("La imange no se debe borrar", imagenOriginal.isFile());
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void actualizar_sinLoguear_lanzaAccessDeniedException() {
        instancia.actualizar(new Menu(), null);
    }

    @Test
    @DirtiesContext
    public void borrar_conIdDeMenuConImagen_borraMenuDeBDEImagen() throws IOException {
        //setUP
        InputStream inputStreamTest = getClass().getResourceAsStream("/img/vianda_1.jpg");
        Long idMenu = 1L;
        Menu menuOriginal = obtenerMenuPorId(idMenu);
        File imagenOriginal = obtenerArchivoImagenDelMenu("kalcero", menuOriginal.getImagen());
        //Creo el archivo de prueba
        FileUtils.copyInputStreamToFile(inputStreamTest, imagenOriginal);

        loginAdministradorDeCasaKalcero();

        //ejercitamos
        instancia.borrar(idMenu);

        //verificamos
        Menu menuBD = obtenerMenuPorId(idMenu);
        assertNull(menuBD);
        assertFalse("La imange no se borro", imagenOriginal.isFile());
    }

    @Test
    @DirtiesContext
    public void borrar_conIdDeMenuSinImagen_borraMenuDeBD() throws IOException {
        Long idMenu = 6L;

        loginAdministradorDeCasaKalcero();
        instancia.borrar(idMenu);

        Menu menuBD = obtenerMenuPorId(idMenu);
        assertNull(menuBD);
    }

    @Test(expected = AccessDeniedException.class)
    public void borrar_conUsuarioLogueadoAdministradorDeOtraCasa_lanzaAccessDeniedException() {
        //setUp
        Long idMenuRatatouille = 4L;
        loginAdministradorDeCasaKalcero();

        //ejercitamos
        instancia.borrar(idMenuRatatouille);
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void borrar_sinLoguear_lanzaAccessDeniedException() {
        instancia.borrar(1L);
    }

    @Test(expected = AccessDeniedException.class)
    public void borrar_sinPermiso_lanzaAccessDeniedException() {
        loginRolObsoleto();
        instancia.borrar(1L);
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void guardar_sinLoguear_lanzaAccessDeniedException() {
        instancia.guardar(new Menu(), null);

    }

    @Test(expected = AccessDeniedException.class)
    public void guardar_sinPermiso_lanzaAccessDeniedException() {
        loginRolObsoleto();
        instancia.guardar(new Menu(), null);

    }

    @Test(expected = AccessDeniedException.class)
    public void actualizar_sinPermiso_lanzaAccessDeniedException() {
        loginRolObsoleto();
        instancia.actualizar(new Menu(), null);
    }


    private File obtenerArchivoImagenDelMenu(String codigoCasa, String nombreImagen) {
        StringBuilder rutaImagen = new StringBuilder();
        rutaImagen.append(pathDirectorioCasa).append("/").append(codigoCasa).append("/").append(pathImagenMenu).append("/").append(nombreImagen);
        File imagen = new File(rutaImagen.toString());
        return imagen;
    }

    private Menu obtenerMenuPorId(Long id) {
        String querySql = "SELECT * FROM cec_menu WHERE id = ?";
        Menu menu = null;
        try {
            menu = (Menu) jdbcTemplate.queryForObject(querySql, new Object[]{id}, new RowMapper() {
                @Override
                public Object mapRow(ResultSet rs, int i) throws SQLException {
                    Menu menuEncontrado = new Menu();
                    Casa casa = new Casa();
                    menuEncontrado.setDescripcion(rs.getString("descripcion"));
                    menuEncontrado.setId(rs.getLong("id"));
                    menuEncontrado.setImagen(rs.getString("imagen"));
                    menuEncontrado.setNombre(rs.getString("nombre"));
                    casa.setId(rs.getLong("id_casa"));
                    menuEncontrado.setCasa(casa);
                    return menuEncontrado;
                }
            });
        } catch (EmptyResultDataAccessException ex) {

        }
        return menu;
    }
}
