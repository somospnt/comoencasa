package comoencasa.service;

import comoencasa.ApplicationConfig;
import comoencasa.domain.Casa;
import comoencasa.domain.Usuario;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = ApplicationConfig.class)
@ActiveProfiles("test")
@WebAppConfiguration
public class CasaServiceTest {

    @Autowired
    private CasaService instancia;

    @Test
    public void buscar_conDosCasaExistentes_traeTodasLasCasas() {
        //setUp
        int canrtidadCasasEsperadas = 3;

        //ejercitamos
        List<Casa> resultado = instancia.buscar();

        //verificamos
        Assert.assertNotNull(resultado);
        Assert.assertEquals(canrtidadCasasEsperadas, resultado.size());
    }

    @Test
    public void buscar_conCodigoDeCasaExistente_retornaCasaDelCodigo() {
        //setUp
        String codigoCasa = "kalcero";

        //ejercitamos
        Casa resultado = instancia.buscar(codigoCasa);

        //verificamos
        Assert.assertNotNull(resultado);
        Assert.assertEquals(codigoCasa, resultado.getCodigo());
    }

    @Test
    public void buscar_conCodigoDeCasaInexsitente_retornaNull() {
        //setUp
        String codigoCasa = "codigo-inexistente";

        //ejercitamos
        Casa resultado = instancia.buscar(codigoCasa);

        //verificamos
        Assert.assertNull(resultado);
    }
    
    @Test
    public void buscarPorUsuario_conUsuarioLogueado_retornaCasa() {
        //setUp
        Usuario usuario = new Usuario();
        

        //ejercitamos
        //Casa resultado = instancia.buscarPorUsuario();

        //verificamos
        //Assert.assertNull(resultado);
    }
}
