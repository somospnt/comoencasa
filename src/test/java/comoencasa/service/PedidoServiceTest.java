package comoencasa.service;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetupTest;
import comoencasa.ComoEnCasaAbstractTest;
import static org.junit.Assert.*;
import comoencasa.domain.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;

public class PedidoServiceTest extends ComoEnCasaAbstractTest {

    @Autowired
    private PedidoService instancia;

    @Resource
    private JavaMailSenderImpl emailSender;

    private GreenMail testSmtp;

    @Autowired
    private DataSource dataSource;

    protected JdbcTemplate jdbcTemplate;

    @Before
    public void init() {
        jdbcTemplate = new JdbcTemplate(dataSource);

        testSmtp = new GreenMail(ServerSetupTest.SMTP);
        testSmtp.start();

        //don't forget to set the test port!
        emailSender.setPort(3025);
        emailSender.setHost("localhost");
    }

    @After
    public void cleanup() {
        testSmtp.stop();
    }


    @Test
    public void guardar_conPedidoCorrectoYUsuarioExistente_guardaPedidoYLoRetorna() {
        //setup
        String sqlPedido = "SELECT COUNT(*) FROM CEC_PEDIDO";
        String sqlItemPedido = "SELECT COUNT(*) FROM cec_pedido_item";

        Integer cantidadPedidosAntes = jdbcTemplate.queryForObject(sqlPedido, Integer.class);
        Integer cantidadItemPedidosAntes = jdbcTemplate.queryForObject(sqlItemPedido, Integer.class);

        Usuario usuario = new Usuario();
        usuario.setId(1L);

        Menu menuUno = new Menu();
        menuUno.setId(1L);

        Menu menuDos = new Menu();
        menuDos.setId(2L);

        PedidoItem pedidoItemUno = new PedidoItem();
        pedidoItemUno.setMenu(menuUno);
        pedidoItemUno.setCantidad(10);

        PedidoItem pedidoItemDos = new PedidoItem();
        pedidoItemDos.setMenu(menuDos);
        pedidoItemDos.setCantidad(10);

        ArrayList<PedidoItem> pedidoItems = new ArrayList<>();
        pedidoItems.add(pedidoItemUno);
        pedidoItems.add(pedidoItemDos);

        Pedido pedido = new Pedido();
        pedido.setIdCasa(1L);
        pedido.setUsuario(usuario);
        pedido.setItems(pedidoItems);

        //ejercitamos
        Pedido pedidoGuardado = instancia.guardar(pedido);

        //post SetUp
        Integer cantidadPedidoDespues = jdbcTemplate.queryForObject(sqlPedido, Integer.class);
        Integer cantidadItemPedidosDespues = jdbcTemplate.queryForObject(sqlItemPedido, Integer.class);

        //verificamos
        assertNotNull(pedidoGuardado);
        assertNotNull(pedidoGuardado.getId());
        assertEquals((cantidadPedidosAntes + 1), cantidadPedidoDespues.intValue());
        assertEquals((cantidadItemPedidosAntes + 2), cantidadItemPedidosDespues.intValue());
        assertEquals(EstadoPedido.PENDIENTE, pedidoGuardado.getEstado());
    }

    @Test
    public void guardar_conPedidoCorrectoYErrorEnElEnvioDeMail_guardaPedidoYActualizaUsuarioYRetornaPedido() {
        //setup
        String sqlPedido = "SELECT COUNT(*) FROM CEC_PEDIDO";
        String sqlItemPedido = "SELECT COUNT(*) FROM cec_pedido_item";

        Integer cantidadPedidosAntes = jdbcTemplate.queryForObject(sqlPedido, Integer.class);
        Integer cantidadItemPedidosAntes = jdbcTemplate.queryForObject(sqlItemPedido, Integer.class);

        Usuario usuario = new Usuario();
        usuario.setId(1L);

        Menu menuUno = new Menu();
        menuUno.setId(1L);

        PedidoItem pedidoItemUno = new PedidoItem();
        pedidoItemUno.setMenu(menuUno);
        pedidoItemUno.setCantidad(10);

        ArrayList<PedidoItem> pedidoItems = new ArrayList<>();
        pedidoItems.add(pedidoItemUno);

        Pedido pedido = new Pedido();
        pedido.setIdCasa(1L);
        pedido.setUsuario(usuario);
        pedido.setItems(pedidoItems);

        //ejercitamos
        testSmtp.stop();
        Pedido pedidoGuardado = instancia.guardar(pedido);

        //post SetUp
        Integer cantidadPedidoDespues = jdbcTemplate.queryForObject(sqlPedido, Integer.class);
        Integer cantidadItemPedidosDespues = jdbcTemplate.queryForObject(sqlItemPedido, Integer.class);

        //verificamos
        assertNotNull(pedidoGuardado);
        assertNotNull(pedidoGuardado.getId());
        assertEquals((cantidadPedidosAntes + 1), cantidadPedidoDespues.intValue());
        assertEquals((cantidadItemPedidosAntes + 1), cantidadItemPedidosDespues.intValue());
        assertEquals(EstadoPedido.PENDIENTE, pedidoGuardado.getEstado());
    }
    
    @Test
    public void buscarOrdenadoPorFecha_conIdCasaConPedidos_retornaListaDePedidos() {
        Long idCasa = 3L;
        
        login("pedidoTest@mail.com");
        List<Pedido> pedidos = instancia.buscarOrdenadoPorFechaDesc(idCasa);
    
        assertNotNull(pedidos);
        assertEquals(3, pedidos.size());
            
        Pedido pedidoAnterior = null;
        for(Pedido pedido : pedidos) {
            if (pedidoAnterior != null) {
                if (pedidoAnterior.getFecha().compareTo(pedido.getFecha()) <  0) {
                    fail("Los pedidos no estan ordenados de forma descendente");
                }
            }
        }
    }
    
    @Test
    public void buscarPorEstadoOrdenadoPorFecha_conIdCasaConPedidosPendientes_retornaListaDePedidosPendientes() {
        Long idCasa = 3L;
        
        login("pedidoTest@mail.com");
        List<Pedido> pedidos = instancia.buscarPorEstadoOrdenadoPorFechaDesc(idCasa, EstadoPedido.PENDIENTE);
    
        assertNotNull(pedidos);
        assertEquals(2, pedidos.size());
            
        Pedido pedidoAnterior = null;
        for(Pedido pedido : pedidos) {
            if (pedidoAnterior != null) {
                if (pedidoAnterior.getFecha().compareTo(pedido.getFecha()) <  0) {
                    fail("Los pedidos no estan ordenados de forma descendente");
                }
            }
        }
    }
    
    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void buscarOrdenadoPorFecha_sinLoguear_lanzaAccessDeniedException() {        
        instancia.buscarOrdenadoPorFechaDesc(3L);
    }
    @Test(expected = AccessDeniedException.class)
    public void buscarOrdenadoPorFecha_sinSerAdmin_lanzaAccessDeniedException() {
        loginRolObsoleto();
        instancia.buscarOrdenadoPorFechaDesc(3L);
    }
    
    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void buscarPorEstadoOrdenadoPorFecha_sinLoguear_lanzaAccessDeniedException() {        
        instancia.buscarOrdenadoPorFechaDesc(3L);
    }
    @Test(expected = AccessDeniedException.class)
    public void buscarPorEstadoOrdenadoPorFecha_sinSerAdmin_lanzaAccessDeniedException() {
        loginRolObsoleto();
        instancia.buscarOrdenadoPorFechaDesc(3L);
    }
}
