package comoencasa.service;

import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.GreenMailUtil;
import com.icegreen.greenmail.util.ServerSetupTest;
import comoencasa.ApplicationConfig;
import comoencasa.domain.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.annotation.Resource;
import javax.mail.Address;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = {ApplicationConfig.class})
@ActiveProfiles("test")
@WebAppConfiguration
public class NotificacionServiceTest {

    @Resource
    private JavaMailSenderImpl emailSender;

    private GreenMail testSmtp;

    @Autowired
    private CasaService casaService;
    
    @Autowired
    private NotificacionService instancia;

    @Autowired
    private DataSource dataSource;

    protected JdbcTemplate jdbcTemplate;

    @Before
    public void testSmtpInit() {
        jdbcTemplate = new JdbcTemplate(dataSource);
        testSmtp = new GreenMail(ServerSetupTest.SMTP);
        testSmtp.start();

        //don't forget to set the test port!
        emailSender.setPort(3025);
        emailSender.setHost("localhost");
    }

    @After
    public void cleanup() {
        testSmtp.stop();
    }

    @Test
    public void pedidosPendientes_conCasaConPedidosPendientes_enviaMailACasaActualizaPedidosAInformado() throws MessagingException {
        String codigoCasa = "kalcero";
        Casa casa = casaService.buscar(codigoCasa);

        String sqlPedidosCasaInformados = "SELECT COUNT(*) FROM CEC_PEDIDO WHERE ID_CASA = " + casa.getId() + " AND ESTADO = 'INFORMADO'";
        Integer cantidadPedidosAntes = jdbcTemplate.queryForObject(sqlPedidosCasaInformados, Integer.class);

        instancia.pedidosPendientes(casa);

        Integer cantidadPedidosDespues = jdbcTemplate.queryForObject(sqlPedidosCasaInformados, Integer.class);
        assertTrue("No se actualizaron los estados de los pedidos", cantidadPedidosDespues > cantidadPedidosAntes);

        Message[] messages = testSmtp.getReceivedMessages();
        assertEquals(1, messages.length);
        assertEquals("Resumen de pedidos - " + casa.getNombre(), messages[0].getSubject());
        String body = GreenMailUtil.getBody(messages[0]).replaceAll("=\r?\n", "");
        assertTrue("El mail no informa el nombre del usuario", body.contains("Maro"));
        assertTrue("El mail no informa el nombre de un menu", body.contains("Vianda oriental"));
        assertTrue("El mail no informa el nombre de la casa", body.contains(casa.getNombre()));
    }
    
    
    @Test
    public void pedido_conPedidoValido_enviaMailALUsuario() throws MessagingException {
        Pedido pedido = new Pedido();
        List<PedidoItem> listaItem = new ArrayList<PedidoItem>();
        PedidoItem item1 = new PedidoItem();
        Menu menu1 = new Menu();
        menu1.setNombre("Milanesa con pure");
        menu1.setDescripcion("Rica milanesa de ternera con pure de papa");
        item1.setMenu(menu1);
        item1.setCantidad(20);
        
        PedidoItem item2 = new PedidoItem();
        Menu menu2 = new Menu();
        menu2.setNombre("Asado con frita");
        menu2.setDescripcion("Parrillada completa una porcion comen 2.");
        item2.setMenu(menu2);
        item2.setCantidad(10);
      
        listaItem.add(item1);
        listaItem.add(item2);
        Usuario usuario = new Usuario();
        usuario.setNombre("Juan");
        usuario.setMail("test@mail.com");
        
        
        pedido.setUsuario(usuario);
        pedido.setItems(listaItem);
        pedido.setIdCasa(1L);
        
        instancia.pedido(pedido);


        Message[] messages = testSmtp.getReceivedMessages();
        assertEquals(1, messages.length);
        //assertEquals("Resumen de pedidos - " + casa.getNombre(), messages[0].getSubject());
        String body = GreenMailUtil.getBody(messages[0]).replaceAll("=\r?\n", "");        
        
        assertEquals("Pedido realizado en KalCero", messages[0].getSubject());
        Address[] direcciones = messages[0].getAllRecipients();
        assertEquals("test@mail.com", direcciones[0].toString());        
        assertTrue("El mail no informa el nombre de un menu 1", body.contains("Milanesa con pure"));
        assertTrue("El mail no informa la descripcion del menu 1", body.contains("Rica milanesa de ternera con pure de papa"));
        assertTrue("El mail no informa la cantidad del menu", body.contains("20"));
        
        assertTrue("El mail no informa el nombre de un menu 2", body.contains("Asado con frita"));
        assertTrue("El mail no informa la descripcion del  menu 2", body.contains("Parrillada completa una porcion comen 2."));
        assertTrue("El mail no informa la cantidad del menu", body.contains("20"));
    }

}
