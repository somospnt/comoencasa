package comoencasa.service;

import comoencasa.ComoEnCasaAbstractTest;
import comoencasa.domain.Cliente;
import comoencasa.domain.Usuario;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.test.annotation.DirtiesContext;

import java.util.List;

public class ClienteServiceTest extends ComoEnCasaAbstractTest {

    @Autowired
    private ClienteService instancia;


    @Test
    @DirtiesContext
    public void generarCliente_conUsuarioYCasaCorrectos_generaElCliente() {
        //setUp
        Usuario usuario = new Usuario();
        usuario.setId(1L);
        Long idCasa = 1L;
        String queryContarClientes = "SELECT count(*) from cec_cliente where id_casa=" + idCasa;

        int cantidadClientesAntes = jdbcTemplate.queryForObject(queryContarClientes, Integer.class);

        //ejercitamos
        instancia.generarCliente(usuario, idCasa);

        //postSetUp
        int cantidadClientesDespues = jdbcTemplate.queryForObject(queryContarClientes, Integer.class);

        //verificamos
        Assert.assertEquals(cantidadClientesAntes + 1, cantidadClientesDespues);
    }

    @Test (expected = DataIntegrityViolationException.class)
    public void generarCliente_conUsuarioInexistente_lanzaDataIntegrityViolationException() {
        //setUp
        Long idInexistente = 9999L;
        Usuario usuario = new Usuario();
        usuario.setId(idInexistente);
        Long idCasa = 1L;

        //ejercitamos
        instancia.generarCliente(usuario, idCasa);
    }

    @Test (expected = DataIntegrityViolationException.class)
    public void generarCliente_conCasaInexistente_lanzaDataIntegrityViolationException() {
        //setUp
        Usuario usuario = new Usuario();
        usuario.setId(1L);
        Long idCasainexistente = 9999L;

        //ejercitamos
        instancia.generarCliente(usuario, idCasainexistente);
    }

    @Test (expected = IllegalArgumentException.class)
    public void generarCliente_conUsuarioNull_lanzaIllegalArgumentException() {
        //setUp
        Usuario usuario = null;
        Long idCasa = 1L;

        //ejercitamos
        instancia.generarCliente(usuario, idCasa);
    }

    @Test (expected = IllegalArgumentException.class)
    public void generarCliente_conCasaNull_lanzaIllegalArgumentException() {
        //setUp
        Usuario usuario = new Usuario();
        usuario.setId(1L);
        Long idCasainexistente = null;

        //ejercitamos
        instancia.generarCliente(usuario, idCasainexistente);
    }

    @Test
    public void generarCliente_conClienteExistente_noInsertaNiFalla() {
        //setUp
        Usuario usuario = new Usuario();
        usuario.setId(2L);
        Long idCasa = 2L;
        String queryContarClientes = "SELECT count(*) from cec_cliente where id_casa=" + idCasa;

        int cantidadClientesAntes = jdbcTemplate.queryForObject(queryContarClientes, Integer.class);

        //ejercitamos
        instancia.generarCliente(usuario, idCasa);

        //postSetUp
        int cantidadClientesDespues = jdbcTemplate.queryForObject(queryContarClientes, Integer.class);

        //verificamos
        Assert.assertEquals(cantidadClientesAntes, cantidadClientesDespues);
    }

    @Test
    public void buscar_conClientesExistentesYUsuarioLogueado_retornaTodosLosClientes() {
        //setUp
        loginAdministradorDeCasaKalcero();
        Long idCasaKalcero = 1L;
        String queryContarClientes = "SELECT count(*) from cec_cliente where id_casa=" + idCasaKalcero;
        int cantidadClientesParaCasa = jdbcTemplate.queryForObject(queryContarClientes, Integer.class);

        //verificamos
        List<Cliente> resultado = instancia.buscar();

        //verificamos
        Assert.assertNotNull(resultado);
        Assert.assertFalse(resultado.isEmpty());
        Assert.assertEquals(cantidadClientesParaCasa, resultado.size());
        for (Cliente cliente : resultado) {
            Assert.assertNotNull(cliente);
            Assert.assertNotNull(cliente.getFechaAlta());
            Assert.assertNotNull(cliente.getId());
            Assert.assertNotNull(cliente.getNotas());
            Assert.assertNotNull(cliente.getUsuario());
            Assert.assertNotNull(cliente.getIdCasa());
            Assert.assertEquals(idCasaKalcero, cliente.getIdCasa());
        }

    }

    @Test(expected = AccessDeniedException.class)
    public void buscar_conUsuarioLogueadoSinRolCorrespondiente_lanzaAccessDeniedException() {
        //setUp
        loginRolObsoleto();

        //verificamos
        instancia.buscar();
    }

    @Test(expected = AuthenticationCredentialsNotFoundException.class)
    public void buscar_sinUsuarioLogueado_lanzaAccessDeniedException() {
        //verificamos
        instancia.buscar();
    }

    @Test
    public void actualizarNotas_conClienteCorrecto_actualizaNotasYRetornaElCliente() {
        //setUp
        login("ratatouille@mail.com");
        String notas = "Una actualización" + (int) (Math.random() * 1000);

        String queryContarClientes = "SELECT count(*) from cec_cliente where notas='" + notas + "'";
        int cantidadClientesAntes = jdbcTemplate.queryForObject(queryContarClientes, Integer.class);

        Cliente cliente = new Cliente();
        cliente.setId(1L);
        cliente.setNotas(notas);

        //ejercitamos
        instancia.actualizarNotas(cliente);

        //postSetUp
        int cantidadClientesDespues = jdbcTemplate.queryForObject(queryContarClientes, Integer.class);

        //verificamos
        Assert.assertEquals(cantidadClientesAntes + 1, cantidadClientesDespues);
    }

    @Test (expected = AccessDeniedException.class)
    public void actualizarNotas_conUsuarioLogueadoIncompatible_lanzaAccesDeniedException() {
        //setUp
        loginAdministradorDeCasaKalcero();
        String notas = "Una actualización" + (int) (Math.random() * 1000);

        Cliente cliente = new Cliente();
        cliente.setId(1L);
        cliente.setNotas(notas);

        //ejercitamos
        instancia.actualizarNotas(cliente);
    }

    @Test(expected = AccessDeniedException.class)
    public void actualizarNotas_conUsuarioLogueadoSinRolCorrespondiente_lanzaAccessDeniedException() {
        //setUp
        loginRolObsoleto();
        String notas = "Una actualización" + (int) (Math.random() * 1000);

        Cliente cliente = new Cliente();
        cliente.setId(1L);
        cliente.setNotas(notas);

        //verificamos
        instancia.actualizarNotas(cliente);
    }
}
