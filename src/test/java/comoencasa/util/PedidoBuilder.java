package comoencasa.util;

import comoencasa.domain.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by semurua on 19/09/14.
 */
public class PedidoBuilder {

    public Long id;
    public Date fecha;
    public Long idCasa;
    public Usuario usuario;
    public EstadoPedido estado;
    public List<PedidoItem> items;

    private PedidoBuilder() {

    }

    public static PedidoBuilder pedidoTipico() {
        PedidoBuilder pedidoBuilder = new PedidoBuilder();
        pedidoBuilder.id = null;
        pedidoBuilder.fecha = new Date();
        pedidoBuilder.idCasa = 1L;
        pedidoBuilder.usuario = null;
        pedidoBuilder.estado = EstadoPedido.PENDIENTE;

        Menu menu = new Menu();
        menu.setId(1L);

        PedidoItem pedidoItem = new PedidoItem();
        pedidoItem.setMenu(menu);
        pedidoItem.setCantidad(10);

        ArrayList<PedidoItem> pedidoItems = new ArrayList<>();
        pedidoItems.add(pedidoItem);

        pedidoBuilder.items = pedidoItems;

        return pedidoBuilder;
    }

    public Pedido build() {
        Pedido pedido = new Pedido();
        pedido.setEstado(this.estado);
        pedido.setFecha(this.fecha);
        pedido.setId(this.id);
        pedido.setIdCasa(this.idCasa);
        pedido.setItems(this.items);
        pedido.setUsuario(this.usuario);

        return pedido;
    }


    public PedidoBuilder conId(Long id) {
        this.id = id;
        return this;
    }

    public PedidoBuilder conFecha(Date fecha) {
        this.fecha = fecha;
        return this;
    }

    public PedidoBuilder conIdCasa(Long idCasa) {
        this.idCasa = idCasa;
        return this;
    }

    public PedidoBuilder con(Usuario usuario) {
        this.usuario = usuario;
        return this;
    }

    public PedidoBuilder con(EstadoPedido estado) {
        this.estado = estado;
        return this;
    }

    public PedidoBuilder con(List<PedidoItem> items) {
        this.items = items;
        return this;
    }
}
